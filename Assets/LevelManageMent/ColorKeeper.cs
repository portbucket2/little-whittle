﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorKeeper : MonoBehaviour
{
    public const bool SHOULD_RANDOMIZE_THEME = true;
    public static ColorKeeper instance;
    public LevelLoader loader;
    public List<ColorTheme> themes;
    public void Awake()
    {
        instance = this;
    }
    public static ColorTheme GetTheme(Theme theme)
    {
        //Debug.Log(theme);

        for (int i = 0; i < instance.themes.Count; i++)
        {
            if (instance.themes[i].theme == theme)
            {
                return instance.themes[i];
            }
        }
        return null;
        //int ti = instance.loader.commonAreaDefinitions[LevelLoader.GetAreaIndex_LastPrefab()].levelData[LevelLoader.GetLevelIndex_LastPrefab()].themeID;
        //return instance.themes[(int)theme];
    }
}

[System.Serializable]
public class ColorTheme
{
    public Theme theme;
    public List<Color> colors;
}