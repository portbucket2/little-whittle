﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


    public class LevelAreaCreator
    {
        [UnityEditor.MenuItem("Assets/Create/LevelArea")]
        public static void Create1()
        {
            LevelAreaDefinition so = ScriptableObject.CreateInstance<LevelAreaDefinition>();
            UnityEditor.AssetDatabase.CreateAsset(so, "Assets/Area X.asset");
            UnityEditor.AssetDatabase.SaveAssets();
            UnityEditor.EditorUtility.FocusProjectWindow();
            UnityEditor.Selection.activeObject = so;
        }

    //[UnityEditor.MenuItem("Assets/Create/LevelArea_CommonPrefab")]
    //public static void Create2()
    //{
    //    AreaData so = ScriptableObject.CreateInstance<AreaData>();
    //    UnityEditor.AssetDatabase.CreateAsset(so, "Assets/Common Area X.asset");
    //    UnityEditor.AssetDatabase.SaveAssets();
    //    UnityEditor.EditorUtility.FocusProjectWindow();
    //    UnityEditor.Selection.activeObject = so;
    //}
}
