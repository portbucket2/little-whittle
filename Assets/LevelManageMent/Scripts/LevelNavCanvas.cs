﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelNavCanvas : MonoBehaviour
{
    public SuccessBoxController successController;
    public GameObject failureObj;
    public GameObject minimizeObj;

    public List<GameObject> stars;



    [Header("Fail")]
    public Button restartButton;
    public Button levelsButton;
    
    //[Header ( "rating" )]
    //public Button giveRating;
    //public Button skipRating;
    //public GameObject ratingwindow;

    [Header("Ext")]
    public Button restartButtonExt;
    public Button levelsButtonExt;
    [Header("Minimize")]
    public Button miniModMinimizeButton;
    //public Button miniModNextButton;
    public GameObject miniModInitialPart;
    public GameObject miniModLaterPart;


    [Header("Decorate")]
    public Button shareOnNetworkButton;
    public Button skipToNextButton;
    public GameObject decoratePanel;

    private LevelPrefabManager levelObject;
    public Text levelNameText;

    public  void LevelStart(LevelPrefabManager levelObj)
    {
        levelObject = levelObj;
        failureObj.SetActive(false);
        miniModInitialPart.SetActive(false);
        miniModLaterPart.SetActive(false);
        decoratePanel.SetActive(false);
        successController.HideAll();

        levelNameText.text = LevelLoader.GetLevelName(levelObj.areaI,levelObj.levelI);

        miniModMinimizeButton.onClick.RemoveAllListeners();
        miniModMinimizeButton.onClick.AddListener(OnMini);

        restartButton.onClick.RemoveAllListeners();
        restartButton.onClick.AddListener(levelObject.OnReset);


        levelsButton.onClick.RemoveAllListeners();
        levelsButton.onClick.AddListener(levelObject.OnLevels);


        restartButtonExt.onClick.RemoveAllListeners();
        restartButtonExt.onClick.AddListener(levelObject.OnReset);

        levelsButtonExt.onClick.RemoveAllListeners();
        levelsButtonExt.onClick.AddListener(levelObject.OnLevels);
        SetStars(0);

        levelsButtonExt.gameObject.SetActive(LevelLoader.instance.testMode);

        shareOnNetworkButton.onClick.RemoveAllListeners();
        shareOnNetworkButton.onClick.AddListener(OnShareButton);

        skipToNextButton.onClick.RemoveAllListeners();
        skipToNextButton.onClick.AddListener(levelObject.OnNext);

    }

    void OnShareButton()
    {
        Debug.Log("<color='cyan'>Sharing to facebook: needs implementation</color>");
        //Portbliss.Social.GifShareControl.EndRecord();
        //Portbliss.Social.GifShareControl.ShareGif();
    }

    public void OnDecorationComplete()
    {
        decoratePanel.SetActive(true);
    }



    public void SetStars(int starCount)
    {
        for (int i = 0; i < stars.Count; i++)
        {
            stars[i].SetActive(starCount > i);
        }
    }


    private void OnMini()
    {

        successController.HideAll();
        miniModInitialPart.SetActive(false);
        miniModLaterPart.SetActive(true);
        onMiniAction?.Invoke();
    }
    public void LoadFail()
    {
        failureObj.SetActive(true);
    }
    System.Action onMiniAction;
    public void LoadSucces(string infoString, System.Action onMiniAction)
    {
        this.onMiniAction = onMiniAction;
        miniModLaterPart.SetActive(false);

        successController.Show(levelObject.OnNext);
    }


    //public void LoadRatingWindow ()
    //{
    //    ratingwindow.SetActive ( true );

    //    skipRating.onClick.AddListener ( () =>
    //    {
    //        ratingwindow.SetActive ( false );
    //    } );
    //    giveRating.onClick.AddListener ( () =>
    //    {
    //        Application.OpenURL ( "market://details?id=" + Application.identifier + "&reviewId=0" );
    //        ratingwindow.SetActive ( false );
    //    } );

    //}
}
