﻿//using Portbliss.Ad;
//using Portbliss.IAP;
using System;
using System.Collections;
using System.Collections.Generic;
using FRIA;
using UnityEngine;

public class MainGameManager : MonoBehaviour
{
    public static QuickSettings settings;
    public static MainGameManager instance;
    public LevelNavCanvas levelCanvas;
    public GameObject loaderObjects;
    public List<GameObject> levelObjects;

    public AutoAreaLoader areaLoader;
    public QuickSettings settingsRef;
    public GameObject singularPlayButton;
    public GameObject multiplePlayArray;
    //public static float firstBootTime { get; private set; }
    //public static HardData<bool> ratingShown;
    //public static bool willShowAd_IAP { get; set; }
    void Awake()
    {
        Application.targetFrameRate = 300;
        instance = this;
        foreach (var item in levelObjects) item.SetActive(false);
        settings = settingsRef;
        //firstBootTime = Time.time;
        LevelPrefabManager.hasLevelProgressOccured = true;
#if UNITY_EDITOR
        Debug.unityLogger.logEnabled = true;
#else
        Debug.unityLogger.logEnabled = false;
#endif
        //willShowAd_IAP = true;
        //willShowAd_IAP = !(GameConfig.hasIAP_NoAdPurchasedHD.value);
        //ratingShown = new HardData<bool>("RATING_ASKED", false);
    }

    //IEnumerator WaitAndShowBanner()
    //{
    //    while (true)
    //    {
    //        if(AdController.IsSDK_Ready && UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex == 1
    //                && AdController.gdpr_done)
    //        {
    //            break; 
    //        }
    //        yield return null;
    //    }

    //    if (willShowAd_IAP == false)
    //    {
    //        AdController.HideBanner();
    //    }
    //    else if (AdController.IsBanerAdShowing == false)
    //    {
    //        AdController.ShowBanner();
    //    }

    //}
    private void Start()
    {
        //willShowAd_IAP = !(GameConfig.hasIAP_NoAdPurchasedHD.value);
        //if (GameConfig.IAP_PurchasedOrNot_HasBeenCheckedHD.value == false)
        //{
        //    GameConfig.IAP_PurchasedOrNot_HasBeenCheckedHD.value = true;
        //    if (IAP_Controller.instance != null)
        //    {
        //        IAP_Controller.instance.IsNoAdPurchased((success) =>
        //        {
        //            if (success)
        //            {
        //                GameConfig.hasIAP_NoAdPurchasedHD.value = true;
        //                willShowAd_IAP = false;
        //            }
        //        });
        //    }  
        //}

        if (LevelLoader.instance.testMode)
        {
            singularPlayButton.SetActive(false);
            multiplePlayArray.SetActive(true);
        }
        else
        {
            singularPlayButton.SetActive(true);
            multiplePlayArray.SetActive(false);
        }
        //StartCoroutine(WaitAndShowBanner());
    }

    public LevelPrefabManager runningLevelManager
    {
        get
        {
            return LevelPrefabManager.currentLevel;
        }
    }


    public void StartLevel(int areaIndex,int levelIndex, LevelDefinition levelDefinition, GameObject resourcePrefab)
    {

        if (runningLevelManager) runningLevelManager.UnLoad();
        //if(loadType != LoadType.GALLERY) AnalyticsAssistant.LevelStarted(LevelLoader.GetLevelCumulativeNumber(areaIndex, levelIndex),levelDefinition.GetGamePlayType(), levelDefinition.GetLayerCount());
        foreach (var item in levelObjects) item.SetActive(true);
        GameObject prefab = LevelLoader.instance.commonPrefab;
        Instantiate(prefab, Vector3.zero, Quaternion.identity).GetComponent<LevelPrefabManager>().LoadAs( areaIndex,levelIndex, levelDefinition, resourcePrefab, EndLevel);

        loaderObjects.SetActive(false);
    }
    public void EndLevel()
    {
        foreach (var item in levelObjects) item.SetActive(false);
        loaderObjects.SetActive(true);
        areaLoader.RestartLoader();

    }

}

//public class TimedAd
//{
//    public static TimedAd instance;
//    public float lastTime = 0;
//    public static void InitSys()
//    {
//        if (instance == null)
//        {
//            instance = new TimedAd();
//            instance.lastTime = 0;
//        }
//    }


//    static void AdCore(Action<bool> OnComplete)
//    {
//        instance.lastTime = Time.time;
//        IdleChecker.instance.allowPeriodicHand = false;
//        AdController.ShowInterstitialAd((success) =>
//        {
//            instance.lastTime = Time.time;
//            IdleChecker.instance.resetEverything();
//            GameConfig.adWatchCountHD.value = GameConfig.adWatchCountHD.value + 1;
//            AnalyticsAssistant.LogAdWatchOneTimeAppsFlyerIfApplicable(GameConfig.adWatchCountHD.value);
//            OnComplete?.Invoke(success);
//        });
//    }

//    public static void AdIteration(Action<bool> OnComplete)
//    {
//        if (instance == null)
//        {
//            instance = new TimedAd();
//            instance.lastTime = Time.time;
//            return;
//        }

//        bool willPlayAd = false;
//        int lvNumAfter = ABManager.GetValue(ABtype.First_Ad);
//        int firstBootTime = 0;
//        int adDelay = ABManager.GetValue(ABtype.Ad_Frequency);
//        if (Time.time > instance.lastTime + adDelay && LevelLoader.instance.GetLastLevelCumulativeNumber() > lvNumAfter
//            && LevelPrefabManager.hasLevelProgressOccured == true 
//            && Mathf.Abs(MainGameManager.firstBootTime - Time.time) > firstBootTime && GameConfig.hasRewardedVideoAdBeenShown == false)
//        {
//            willPlayAd = true;
//        }

//        if (willPlayAd)
//        {
//            AdCore(OnComplete);
//        }
//        else
//        {
//            Debug.Log("so we wont play ad at this time since condition is not favorable");
//            OnComplete?.Invoke(false);
//        }
//    }
//}