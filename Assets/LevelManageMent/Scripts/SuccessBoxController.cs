﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SuccessBoxController : MonoBehaviour
{
    public Button bannerNextButton;
    //public Button nextWindowButton;
    public Button nextLevelButton;
    


    //public List<GameObject> group1;
    //public List<GameObject> group2;

    public List<Animator> stars;

    //public Image progressImage;
    //public Text progressText;

    //public Text yourTimeText;
    //public Text wolrdTimeText;
    //public Text finalMathText;

    private void Awake()
    {
        //nextWindowButton.onClick.RemoveAllListeners();
        //nextWindowButton.onClick.AddListener(Step);

        nextLevelButton.onClick.RemoveAllListeners();
        nextLevelButton.onClick.AddListener(Next);
        

    }

//    private void OnDecorate()
//    {
//        decorateButton.interactable = false;
//        //AnalyticsAssistant.LogRewardedVideoAdStart(LevelPrefabManager.currentLevel.cumulitiveLevelNo);
//#if UNITY_EDITOR
//        OnAdSuccess(true);
//#else
//        Portbliss.Ad.AdController.ShowRewardedVideoAd(OnAdSuccess);
//#endif
//    }
//    void OnAdSuccess(bool success)
//    {
//        if(!success)
//        {

//            decorateButton.interactable = true;
//            return;
//        }
//        else 
//        {
//            //AnalyticsAssistant.LogRewardedVideoAdComplete(LevelPrefabManager.currentLevel.cumulitiveLevelNo);
//            this.gameObject.SetActive(false);
//            PhaseController_Decorate decPhase = (PhaseManager.CurrentPhaseController as PhaseController_Decorate);
//            if (decPhase)
//            {
//                decPhase.LoadDefaultDecoration(GalleryManager.instance.SaveGalleryItemData());
//            }
//            Portbliss.Social.GifShareControl.BeginRecord();
//        }
//        GameConfig.hasRewardedVideoAdBeenShown = success;
//    }
    System.Action onNextLevel;

    bool showMatchScreen {

        get
        {
            return true;
//            int matchVal = ABManager.GetValue(ABtype.Match_Screen);
//#if UNITY_EDITOR
//            matchVal = -1;
//#endif
            //switch (matchVal)
            //{
            //    case 0:
            //        return false;
            //    default:
            //    case 1:
            //        return true;
            //}
        }
    }
    public void Show(System.Action onNextLevel)
    {

        if (showMatchScreen)
        {
            this.gameObject.SetActive(true);

            this.onNextLevel = onNextLevel;
            //foreach (var item in group1) item.SetActive(true);
            //foreach (var item in group2) item.SetActive(false);

            //float matchRatio = PhaseManager.GetStarProgress();
            //StopAllCoroutines();
            //StartCoroutine(AccuracyCo(matchRatio));

            //finalMathText.text = string.Format("<size=48>Match</size>\n<color=#961672>{0}%</color>", (int)(matchRatio * 100));
            //yourTimeText.text = string.Format("<size=47>Time</size>\n<color=#961672>{0} <size=36>sec</size></color>", PhaseManager.instance.GetAllPhaseTotalTime());
            //wolrdTimeText.text = string.Format("<size=47>World Avg</size>\n<color=#961672>{0} <size=36>sec</size></color>", LevelPrefabManager.currentLevel.levelDefinition.worldAvgTimeText);

            int earnedSTars = 3;//PhaseManager.GetProgressStarCount()
            for (int i = 0; i < earnedSTars; i++)
            {
                stars[i].ResetTrigger("go");
                stars[i].SetTrigger("reset");
            }
            for (int i = 0; i < earnedSTars; i++)
            {
                int index = i;
                FRIA.Centralizer.Add_DelayedMonoAct(this, () => stars[index].SetTrigger("go"), index * 0.5f + 0.1f);
            }
        }
        else
        {
            bannerNextButton.gameObject.SetActive(true);
            bannerNextButton.onClick.RemoveAllListeners();
            bannerNextButton.onClick.AddListener(()=> onNextLevel?.Invoke());
        }
        
    }
    //IEnumerator AccuracyCo(float fraction)
    //{
    //    progressImage.fillAmount = 0;
    //    progressText.text = string.Format("Match: 0%");
    //    yield return new WaitForSeconds(0.5f);
    //    float T = 2f;
    //    float st = Time.time;

    //    float progress;
    //    while (Time.time<st+T)
    //    {
    //        progress = Mathf.Clamp01((Time.time-st)/ T)*fraction;
    //        progressImage.fillAmount = progress;
    //        progressText.text = string.Format("Match: {0}%", (int)(progress*100));
    //        yield return null;
    //    }
    //    progress = fraction;
    //    progressImage.fillAmount = progress;
    //    progressText.text = string.Format("Match: {0}%", (int)(progress*100));
    //}

    //public void Step()
    //{
    //    foreach (var item in group1) item.SetActive(false);
    //    foreach (var item in group2) item.SetActive(true);


    //}

    public void Next()
    {
        HideAll();
        onNextLevel?.Invoke();
    }
    public void HideAll()
    {
        this.gameObject.SetActive(false);
        this.bannerNextButton.gameObject.SetActive(false);

        
    }

    //private void OnDisable()
    //{

    //    foreach (var item in group1) item.SetActive(false);
    //    foreach (var item in group2) item.SetActive(false);
    //}
}
