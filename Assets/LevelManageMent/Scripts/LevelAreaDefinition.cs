﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelAreaDefinition : ScriptableObject
{
    public int starToEnter = 0;
    public List<LevelPrefabManager> levelPrefs;
}

