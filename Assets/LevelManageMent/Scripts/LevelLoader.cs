﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FRIA;
using System;

public class LevelLoader : MonoBehaviour
{
    public bool testMode = false;
    public bool allLevelsUnlocked = false;
    public int numberofexceptions = 40;
    public GameObject commonPrefab;
    [Header("Common Setup")]
    public List<AreaData> areaData;

    internal List<AreaDefinition> areaDefinitions = new List<AreaDefinition>();

    public static LevelLoader instance;

    public HardData<int> randomAreaIndex;
    public HardData<int> randomLevelIndex;

    public List<HardData<int>> randomlevelcollection;
    public HardData<int> LastRandomIndex;

    int csv_choice { get { return 0; } }
    public void Awake()
    {
        if (instance)
        {
            Destroy(this.gameObject);
            return;
        }
        else
        {
            instance = this;
            if (!testMode) allLevelsUnlocked = false;
            randomAreaIndex = new HardData<int>("RAI", 0);
            randomLevelIndex = new HardData<int>("RLI", 0);
            DontDestroyOnLoad(this.transform.root.gameObject);

            for (int i = 0; i < areaData.Count; i++)
            {
                areaDefinitions.Add(new AreaDefinition(areaData[i],i, csv_choice));
            }

            for (int a = 0; a < areaDefinitions.Count; a++)
            {
                AreaDefinition area = areaDefinitions[a];
                area.lastUnlockedLevelIndex = new HardData<int>(string.Format("UNLOCKED_PROGRSS_{0}", a), 0);
                area.stars = new List<HardData<int>>();
                for (int l = 0; l < area.levelDefinitions.Count; l++)
                {
                    area.stars.Add(new HardData<int>(string.Format("stars_{0}_{1}", a, l), 0));
                }
            }
            //Debug.Log("ad count" + areaDefinitions.Count);
            randomlevelcollection = new List<HardData<int>>();

            for (int i = 0; i < numberofexceptions * 2; i++)
            {
                randomlevelcollection.Add(new HardData<int>(string.Format("RANDOM_INDEX_{0}_{1}", (int)(i / 2), i % 2 == 0 ? "AREA" : "LEVEL"), -1));
            }
            LastRandomIndex = new HardData<int>("LAST_INDEX", 0);
        }


    }
    public void AddtoRandomException(int aI, int lI)
    {
        randomlevelcollection[LastRandomIndex.value * 2].value = aI;
        randomlevelcollection[LastRandomIndex.value * 2 + 1].value = lI;
    }

    public void changerandomindex()
    {
        if (LastRandomIndex.value >= numberofexceptions - 1)
        {
            LastRandomIndex.value = 0;
        }
        else
            LastRandomIndex.value++;
    }
    public int GetStarFor(LevelDefinition definition)
    {
        return areaDefinitions[definition.sequence_AI].stars[definition.sequence_LI].value;
    }
    public void SetStarFor(LevelDefinition definition, int star)
    {
        areaDefinitions[definition.sequence_AI].stars[definition.sequence_LI].value = star;
    }
    public int GetTotalStarCount()
    {

        int count = 0;
        foreach (AreaDefinition area in areaDefinitions)
        {
            foreach (HardData<int> star in area.stars)
            {
                count += star.value;
            }
        }
        return count;
    }
    public bool IsAreaUnlocked(int areaIndex)
    {
        if (areaDefinitions[areaIndex].starToEnter > GetTotalStarCount()) return false;
        if (areaIndex == 0) return true;
        else if (areaDefinitions[areaIndex - 1].lastUnlockedLevelIndex.value >= (areaDefinitions[areaIndex - 1].levelDefinitions.Count ))
        {
            return true;
        }
        else return false;
    }

    public static string GetLevelName(int areaI, int levelI)
    {
        return instance.areaDefinitions[areaI].levelDefinitions[levelI].title;

        int cumulativeIndex = 0;
        for (int i = 0; i < areaI; i++)
        {
            cumulativeIndex += instance.areaDefinitions[i].levelDefinitions.Count;
        }
        if (instance != null)
        {
            {

                return string.Format("Level {0}", cumulativeIndex + levelI + 1);

            }
        }
        else
        {
            return string.Format("Level {0}-{1}", areaI + 1, levelI + 1);
        }
    }
    public static int GetLevelCumulativeNumber(int areaI, int levelI)
    {
        if (instance != null)
        {
            int cumulativeIndex = 0;
            for (int i = 0; i < areaI; i++)
            {
                cumulativeIndex += instance.areaDefinitions[i].levelDefinitions.Count;
            }
            return cumulativeIndex + levelI + 1;
        }
        else
        {
            return levelI + 1;
        }
    }
    public int GetLastLevelCumulativeNumber()
    {
        return GetLevelCumulativeNumber(Last_ai, Last_li);
    }
    public void LoadTheLatestLevel()
    {
        LoadResourceAndLevel_Normal(Last_ai,Last_li);
    }
    public void LoadResourceAndLevel_Normal(int ai, int li)
    {
        LevelDefinition definition = FetchAppropriateDefinition(ai, li);
        GameObject resource = Resources.Load<GameObject>(definition.resourcePath);
        //Debug.Log(definition.resourcePath);
        MainGameManager.instance.StartLevel(ai,li, definition, resource);
    }

    public static LevelDefinition FetchAppropriateDefinition(int ai, int li)
    {

        if (ai == instance.areaDefinitions.Count - 1)
        {
            if (li >= instance.areaDefinitions[ai].levelDefinitions.Count)
            {
                ai = instance.randomAreaIndex.value;
                li = instance.randomLevelIndex.value;
            }
        }

        return instance.areaDefinitions[ai].levelDefinitions[li];
    }

    public static int Last_ai
    {
        get
        {

            if (!instance) return -1;


            int lastProperAreaIndex = 0;
            for (int i = 0; i < instance.areaDefinitions.Count; i++)
            {
                AreaDefinition area = instance.areaDefinitions[i];
                if (!instance.IsAreaUnlocked(i))
                {
                    return lastProperAreaIndex;
                }
                else if (area.levelDefinitions.Count > 0)
                {
                    lastProperAreaIndex = i;
                }
            }
            if (instance.areaDefinitions.Count == 0) return -1;
            else return lastProperAreaIndex;
        }
    }
    public static int Last_li
    {
        get
        {
            int ai = Last_ai;
            return instance.areaDefinitions[ai].lastUnlockedLevelIndex.value;
        }
    }

    public static int GetNextLevel_ai(int current_ai, int current_li)
    {
        AreaDefinition currentArea = instance.areaDefinitions[current_ai];
        if (current_li + 1 < currentArea.levelDefinitions.Count)
        {
            return current_ai;
        }
        else
        {
            if (current_ai + 1 < instance.areaDefinitions.Count)
            {
                return current_ai + 1;
            }
            else
            {
                return current_ai;
            }
        }
    }
    public static int GetNextLevel_li(int current_ai, int current_li)
    {
        AreaDefinition currentArea = instance.areaDefinitions[current_ai];
        if (current_li + 1 < currentArea.levelDefinitions.Count)
        {
            return current_li + 1;
        }
        else
        {
            if (current_ai + 1 < instance.areaDefinitions.Count)
            {
                return 0;
            }
            else
            {
                return current_li + 1;
            }
        }
    }

}

[System.Serializable]
public class AreaData
{
    public int starToEnter = 0;
    public TextAsset[] sequenceAndVariantData_CSV;
}
public class AreaDefinition
{
    public int starToEnter=0;
    public List<LevelDefinition> levelDefinitions;

    public AreaDefinition(AreaData data, int areaI, int csvChoice)
    {
        if (data == null) Debug.LogError("null prefab");

        starToEnter = data.starToEnter;
        levelDefinitions = new List<LevelDefinition>();
        CSVRow[] sequenceData = CSVReader.ReadCSVAsset(data.sequenceAndVariantData_CSV[csvChoice], '|');
        //Debug.Log(sequenceData.Length + " data length");
        for (int sequence_LI = 0; sequence_LI < sequenceData.Length; sequence_LI++)
        {
            levelDefinitions.Add(new LevelDefinition(sequenceData[sequence_LI], areaI,sequence_LI));
        }
    }

    [NonSerialized]
    public List<HardData<int>> stars;
    [NonSerialized]
    public HardData<int> lastUnlockedLevelIndex;
}

public class LevelDefinition
{

    public string title;
    public int initialColorIndex;
    public int sequence_AI;
    public int sequence_LI;

    public int areaPathID;
    //public string phaseSequenceID;
    public string levelID;
    //public string seamTypeID;
    //public string colorVariantID;
    //public string decorationID;
    //public string decorationColorVariantID;
    //public string dataFolder_subPath;
    public string resourcePath;
    //public string resourcePath_dataFolder;

    public string worldAvgTimeText;

    public LevelDefinition(CSVRow levelData, int areaI,int sequence_LI)
    {
        this.sequence_AI = areaI;
        this.sequence_LI = sequence_LI;
        initialColorIndex = 0;

        areaPathID = areaI + 1;
        for (int i = 0; i < levelData.fields.Length; i++)
        {
            switch (i)
            {
                case 0:
                    levelID = levelData.fields[i];
                    break;
                case 1:
                    title = levelData.fields[i];
                    break;
            }
        }
        resourcePath = string.Format("Area {0}/{1}", areaPathID, levelID);
        //resourcePath_dataFolder = string.Format("Area {0}/{1}_{2}/{3}", areaPathID, baseBreakdownID, phaseSequenceID, dataFolder_subPath);
    }
    public string GetAssetPathForDecoID(string decoID)
    {
        return string.Format("Decorations/{0}/{1}", levelID, decoID);
    }
}
