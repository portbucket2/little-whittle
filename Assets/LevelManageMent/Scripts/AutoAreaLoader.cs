﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AutoAreaLoader : MonoBehaviour
{
    public int areaIndex;
    public AreaLoader loaderRef;

    public Button prevAreaButton;
    public Button nextAReaButton;
    public Button resetButton;



    void Start()
    {

        resetButton.onClick.RemoveAllListeners();
        resetButton.onClick.AddListener(Reset);

        prevAreaButton.onClick.RemoveAllListeners();
        prevAreaButton.onClick.AddListener(OnPrev);

        nextAReaButton.onClick.RemoveAllListeners();
        nextAReaButton.onClick.AddListener(OnNext);

        RestartLoader();
    }

    public void RestartLoader()
    {
        areaIndex = LevelLoader.Last_ai;
        RefreshLoader();
    }


    private void OnNext()
    {
        areaIndex++;
        RefreshLoader();
    }
    private void OnPrev()
    {
        areaIndex--;
        RefreshLoader();
    }
    private void RefreshLoader()
    {
        loaderRef.Load(areaIndex);
        if (areaIndex - 1 >= 0)
        {

            prevAreaButton.gameObject.SetActive(true);
            prevAreaButton.interactable = LevelLoader.instance.IsAreaUnlocked(areaIndex - 1);
        }
        else
        {
            prevAreaButton.gameObject.SetActive(false);
        }

        if (areaIndex + 1 < LevelLoader.instance.areaDefinitions.Count)
        {
            nextAReaButton.gameObject.SetActive(true);
            nextAReaButton.interactable = LevelLoader.instance.IsAreaUnlocked(areaIndex + 1);
        }
        else
        {
            nextAReaButton.gameObject.SetActive(false);
        }

    }

    private void Reset()
    {
        PlayerPrefs.DeleteAll();
        Application.LoadLevel(Application.loadedLevelName);
    }
}
