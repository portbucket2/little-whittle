﻿using System;
using System.Collections;
using System.Collections.Generic;
//using Portbliss.IAP;
using FRIA;
using UnityEngine;

public class LevelPrefabManager : MonoBehaviour
{
    public const float progressForStar1 = 0.40f;
    public const float progressForStar2 = 0.70f;
    public const float progressForStar3 = 0.95f;
    public const bool shouldUpdateStarsAtProgressReport = true;
    public const bool shouldReportCompleteWhenStarEarned = true;

    internal Theme theme = Theme.BLUE;
    public static event System.Action onLoad;
    public static event System.Action onUnload;
    public static event System.Action<int> onStarCountChanged;
    public static event System.Action<float> onProgressChanged;
    public static event System.Action<Theme> onThemeChanged;
    public static LevelPrefabManager currentLevel;

    [Range(0, 1)]
    public float progress = 0;
    


    [HideInInspector] public bool suspendedExternally;

    public int areaI { get; private set; }
    public int levelI { get; private set; }

    public LevelDefinition levelDefinition;
    public int sequence_areaIndex { get { return levelDefinition.sequence_AI; } }
    public int sequence_levelIndex { get { return levelDefinition.sequence_LI; } }
    public static bool hasLevelProgressOccured = false;
    int stars = 0;
    System.Action onEnd;

    public LevelNavCanvas canvMan
    {
        get
        {
            return MainGameManager.instance.levelCanvas;
        }
    }



    public int cumulitiveLevelNo;

    #region Load Unload
    public void LoadAs( int areaIndex, int levelIndex, LevelDefinition deifinition, GameObject prefab, System.Action onEnd)
    {
        //Debug.Log("LOAD");
        currentLevel = this;
        areaI = areaIndex;
        levelI = levelIndex;
        this.levelDefinition = deifinition;
        this.onEnd = onEnd;
        cumulitiveLevelNo = LevelLoader.GetLevelCumulativeNumber(areaI, levelI);

        
        GameObject innerResource = Instantiate(prefab, Vector3.zero, Quaternion.identity, this.transform);
        IThemeScript themescript = innerResource.GetComponentInChildren<IThemeScript>();

        if (ColorKeeper.SHOULD_RANDOMIZE_THEME)
        {
            theme = (Theme)  UnityEngine.Random.Range(0,System.Enum.GetValues(typeof(Theme)).Length);
            if (themescript != null)
            {
                themescript.SetTheme(theme);
            }
        }
        else
        { 
            if (themescript != null)
            {
                theme = themescript.GetTheme();
            }
        }

        canvMan.LevelStart(this);
        onLoad?.Invoke();
        onThemeChanged?.Invoke(theme);

        ReportProgress(0);
        SetStar(0);

    }

    public void UnLoad()
    {
        //Debug.Log("UN_LOAD");
        onUnload?.Invoke();
        currentLevel = null;
        Destroy(gameObject);
        Resources.UnloadUnusedAssets();
        onEnd?.Invoke();
    }

    #endregion

    #region Gameplay

    public void ReportProgress(float progress)
    {
        progress = Mathf.Clamp01( progress * 1.005f);
        this.progress = progress;
        onProgressChanged?.Invoke(progress);

        if (shouldUpdateStarsAtProgressReport)
        {
            if (progress >= progressForStar3)
            {
                SetStar(3);
            }
            else if (progress >= progressForStar2)
            {
                SetStar(2);
            }
            else if (progress >= progressForStar1)
            {
                SetStar(1);
            }
        }
    }
    public void SetStar(int setStarTo)
    {
        stars = setStarTo;
        SetAllTimeStar_ToAtleastCurrentStarCount();
        canvMan.SetStars(stars);
        if (shouldReportCompleteWhenStarEarned && stars>0)
        {
            //ReportEarlyCompletion(stars);
            LevelRandomizer();
        }

        onStarCountChanged?.Invoke(stars);
    }

    private void SetAllTimeStar_ToAtleastCurrentStarCount()
    {
        int starC = LevelLoader.instance.GetStarFor(levelDefinition);
        if (starC < stars)
        {
            LevelLoader.instance.SetStarFor(levelDefinition, stars);
        }
    }


    //private void ReportEarlyCompletion (int reportedStars)
    //{
    //    int starC = LevelLoader.instance.GetStarFor(levelDefinition);
    //    if (starC < reportedStars)
    //    {
    //        LevelLoader.instance.SetStarFor(levelDefinition, reportedStars);
    //    }
    //    LevelRandomizer();
    //}
    
    public void OnComplete(bool success, string infoText = null, System.Action onMinimize=null)
    {
        if (success)
        {
            int starC = LevelLoader.instance.GetStarFor(levelDefinition);
            if (starC >= stars)
            {
                stars = starC;
            }
            else
            {
                LevelLoader.instance.SetStarFor(levelDefinition, stars);
            }

            LevelRandomizer();
            canvMan.LoadSucces(infoText, onMinimize);
        }
        else
        {
            canvMan.LoadFail();
        }
        canvMan.SetStars(stars);
        //if (loadType != LoadType.GALLERY) AnalyticsAssistant.LevelCompletedAppsFlyerIfItIsProperLevel(cumulitiveLevelNo);
    }

    #endregion

    #region  Level Randomization
    void LevelRandomizer()
    {
        if (!checkcontainment(sequence_areaIndex, sequence_levelIndex))
        {
            LevelLoader.instance.AddtoRandomException(sequence_areaIndex, sequence_levelIndex);
            LevelLoader.instance.changerandomindex();
        }
        if (levelI >= LevelLoader.instance.areaDefinitions[areaI].lastUnlockedLevelIndex.value)
        {
            LevelLoader.instance.areaDefinitions[areaI].lastUnlockedLevelIndex.value = levelI + 1;

            int arI = UnityEngine.Random.Range(0, LevelLoader.instance.areaDefinitions.Count);
            int lvlI = UnityEngine.Random.Range(0, LevelLoader.instance.areaDefinitions[arI].levelDefinitions.Count);

            int j = 0;
            while (checkcontainment(arI, lvlI) && j < 100)
            {
                arI = UnityEngine.Random.Range(0, LevelLoader.instance.areaDefinitions.Count);
                lvlI = UnityEngine.Random.Range(0, LevelLoader.instance.areaDefinitions[arI].levelDefinitions.Count);
                j++;
            }

            LevelLoader.instance.randomAreaIndex.value = arI;
            LevelLoader.instance.randomLevelIndex.value = lvlI;
        }


    }
    bool checkcontainment(int aI, int lI)
    {
        for (int i = 0; i < LevelLoader.instance.numberofexceptions; i++)
        {
            if (LevelLoader.instance.randomlevelcollection[i * 2].value == aI && LevelLoader.instance.randomlevelcollection[i * 2 + 1].value == lI)
            {
                return true;
            }
        }

        return false;
    }
    #endregion

    #region Transition
    public void OnNext()
    {
        if (suspendedExternally) return;
        UnLoad();
        LevelPrefabManager.hasLevelProgressOccured = true;
        /*
        Portbliss.Social.GifShareControl.EndRecord();

        MainGameManager.willShowAd_IAP = !(GameConfig.hasIAP_NoAdPurchasedHD.value);

        if (MainGameManager.willShowAd_IAP)
        {
            try
            {
                TimedAd.AdIteration((success) =>
                {
#if (UNITY_IOS)
                    if (cumulitiveLevelNo >= 3 && !MainGameManager.ratingShown.value)
                    {
                        UnityEngine.iOS.Device.RequestStoreReview();
                        MainGameManager.ratingShown.value = true;
                    }
#endif
                });
            }
            catch (System.Exception e)
            {
                Debug.LogWarning(e.Message);
#if (UNITY_IOS)
                    if (cumulitiveLevelNo >= 3 && !MainGameManager.ratingShown.value)
                    {
                        UnityEngine.iOS.Device.RequestStoreReview();
                        MainGameManager.ratingShown.value = true;
                    }
#endif
            }
        }
        else
        {
#if (UNITY_IOS)
                    if (cumulitiveLevelNo >= 3 && !MainGameManager.ratingShown.value)
                    {
                        UnityEngine.iOS.Device.RequestStoreReview();
                        MainGameManager.ratingShown.value = true;
                    }
#endif
        }


#if (UNITY_ANDROID)
        if (cumulitiveLevelNo >= 3 && !MainGameManager.ratingShown.value)
        {
            MainGameManager.instance.levelCanvas.LoadRatingWindow();
            MainGameManager.ratingShown.value = true;
        }
#endif
        */
        int ai = LevelLoader.GetNextLevel_ai(areaI, levelI);
        int li = LevelLoader.GetNextLevel_li(areaI, levelI);
        LevelLoader.instance.LoadResourceAndLevel_Normal(ai, li);
        //GameConfig.hasRewardedVideoAdBeenShown = false;
    }

    public void OnReset()
    {
        if (suspendedExternally) return;
        hasLevelProgressOccured = false;
        int ai = currentLevel.areaI;
        int li = currentLevel.levelI;
        UnLoad();
        //AnalyticsAssistant.LevelRestarted(cumulitiveLevelNo, levelDefinition.GetGamePlayType(), levelDefinition.GetLayerCount());
        LevelLoader.instance.LoadResourceAndLevel_Normal(ai, li);


    }

    public void OnLevels()
    {
        if (suspendedExternally) return;
        UnLoad();
        
    }
    #endregion
}
public enum Theme
{
    RED = 0,
    GREEN = 1,
    BLUE = 2,
    YELLOW = 3,
}
public interface IThemeScript
{
    Theme GetTheme();
    void SetTheme(Theme theme);
}

