﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using FRIA;

public class RotationSpeedManager : MonoBehaviour
{
    public static RotationSpeedManager instance;
    private Vector3 currentRotationSpeed = new Vector3(0, 135, 0);
    public Vector3 baseRotationSpeed
    {
        get
        {
            if (PeelGenerationManager.instance)
            {
                currentRotationSpeed = Vector3.Lerp(currentRotationSpeed, PeelGenerationManager.instance.GetFinalBaseRotationSpeed(), 5 * Time.deltaTime);
            }
            return currentRotationSpeed;
        }
    }

    static HardData<int> savedSpeedLevel;
    RotationSpeedLevel speedLevel
    {
        get { return (RotationSpeedLevel)savedSpeedLevel.value; }
        set { savedSpeedLevel.value = (int) value; }
    }
    public Transform rotationTarget;

    public Button[] plusbuttons;
    public Image plusMainImage;
    public Image plusChildImage;
    public Button[] minusbuttons;
    public Image minusMainImage;
    public Image minusChildImage;
    public Text speedText;

    float dampingRatio = 0;
    public Vector3 SpeedValueForLevel
    {
        get
        {
            switch (speedLevel)
            {
                case RotationSpeedLevel.pos_050:
                    return baseRotationSpeed * 0.5f;
                case RotationSpeedLevel.pos_100:
                    return baseRotationSpeed * 1.0f;
                case RotationSpeedLevel.pos_150:
                    return baseRotationSpeed * 1.5f;
                case RotationSpeedLevel.pos_200:
                    return baseRotationSpeed * 2.0f;
                case RotationSpeedLevel.pos_075:
                    return baseRotationSpeed * 0.75f;
                default:
                    return baseRotationSpeed;
            }
        }

    }
    public Vector3 ChosenSpeed
    {
        get
        {
            return SpeedValueForLevel*(1-dampingRatio);
        }
    }
    public string speedString
    {
        get
        {
            switch (speedLevel)
            {
                case RotationSpeedLevel.pos_050:
                    return "Speed x0.5";
                case RotationSpeedLevel.pos_100:
                    return "Speed x1";
                case RotationSpeedLevel.pos_150:
                    return "Speed x1.5";
                case RotationSpeedLevel.pos_200:
                    return "Speed x2";
                case RotationSpeedLevel.pos_075:
                    return "Speed x0.75";
                default:
                    return "Speed x1";
            }
        }

    }

    int max;
    private void Awake()
    {
        instance = this;
        dampingRatio = 0;
        if(savedSpeedLevel==null) savedSpeedLevel = new HardData<int>("SPEED_LEVEL", (int)RotationSpeedLevel.pos_100);
        max = System.Enum.GetValues(typeof(RotationSpeedLevel)).Length;

        foreach ( Button b in plusbuttons )
        {
            b.onClick.AddListener(() => OnChoiceChanged(true));
        }
        foreach ( Button b in minusbuttons )
        {
            b.onClick.AddListener ( () => OnChoiceChanged ( false ) );
        }
        if(speedText) speedText.text = speedString;

        int currentIndex = (int)speedLevel;
        foreach ( Button b in plusbuttons )
        {
            b.interactable = currentIndex != max - 1;
        }
        foreach ( Button b in minusbuttons )
        {
            b.interactable = currentIndex != 0;
        }
        if (plusChildImage)
            plusChildImage.color = plusMainImage.color * ( plusbuttons[0].interactable ? Color.white : disabledCol);
        if (minusChildImage)
            minusChildImage.color = minusMainImage.color * (minusbuttons[0].interactable ? Color.white : disabledCol);
    }
    Color disabledCol = new Color(0.5f, 0.5f, 0.5f, 0.5f);

    void OnChoiceChanged(bool positive)
    {
        int currentIndex = (int)speedLevel;

        currentIndex += (positive ? 1 : -1);
        speedLevel = (RotationSpeedLevel)currentIndex;

        if (speedText) speedText.text = speedString;

        foreach ( Button b in plusbuttons )
        {
            b.interactable = currentIndex != max - 1;
        }
        foreach ( Button b in minusbuttons )
        {
            b.interactable = currentIndex != 0;
        }
        if(plusChildImage)
            plusChildImage.color = plusMainImage.color * ( plusbuttons[0].interactable ? Color.white : disabledCol);
        if(minusChildImage)
            minusChildImage.color = minusMainImage.color * ( minusbuttons[0].interactable ? Color.white : disabledCol);

    }

    public static string SpeedMultiplierName()
    {
        switch (instance.speedLevel)
        {
            case RotationSpeedLevel.pos_050:
                return "0.50x";
            case RotationSpeedLevel.pos_075:
                return "0.75x";
            case RotationSpeedLevel.pos_100:
                return "1.00x";
            case RotationSpeedLevel.pos_150:
                return "1.50x";
            case RotationSpeedLevel.pos_200:
                return "2.00x";
            default:
                return "?x";
        }
    }
    
    void Update()
    {
        rotationTarget.transform.Rotate(ChosenSpeed * Time.deltaTime);
    }

    Coroutine rotationBreaker;
    public void BreakRotation(float changingTime,bool reverseBreak)
    {
        if (rotationBreaker!=null) StopCoroutine(rotationBreaker);

        StartCoroutine(RotationBreak(changingTime,reverseBreak));
    }
    IEnumerator RotationBreak(float changingTime, bool reverseBreak)
    {
        float startTime = Time.time;
        while ((dampingRatio <1 && !reverseBreak) || (reverseBreak && dampingRatio >0))
        {
            float timePassed = Time.time - startTime;

            if (!reverseBreak)
                dampingRatio = Mathf.Lerp(0, 1, Mathf.Clamp01(timePassed / changingTime));
            else

                dampingRatio = Mathf.Lerp(1, 0, Mathf.Clamp01(timePassed / changingTime));
            yield return null;
        }
    }

}

public enum RotationSpeedLevel
{
    pos_050,
    pos_075,
    pos_100,
    pos_150,
    pos_200,
}