﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class ColorLoaderImage : MonoBehaviour
{
    public IThemeScript startThemeKeep;
    private Image img;
    public int colorID = 0;

    public float firstFadeTime = -1;
    void Start()
    {
        //int ti = instance.loader.commonAreaDefinitions[LevelLoader.GetAreaIndex_LastPrefab()].levelData[LevelLoader.GetLevelIndex_LastPrefab()].themeID;
        //int ai = LevelLoader.LastPref_ai();
        //int li = LevelLoader.LastPref_li();

        Theme theme = Theme.BLUE;
        if (startThemeKeep == null)
        {
            LevelPrefabManager pref = LevelPrefabManager.currentLevel;
            //GameObject pref = (GameObject)Resources.Load(string.Format("Area {0}/Level {1}", ai + 1, li + 1));
            if (pref != null)
            {
                startThemeKeep = pref.GetComponentInChildren<IThemeScript>();
            }

            //Debug.Log(pref);
            //Debug.Log(ith);
        }
        if (startThemeKeep != null)
        {
            theme = startThemeKeep.GetTheme();
        }

        img = GetComponent<Image>();
        LevelPrefabManager.onThemeChanged += OnNewLoad;

        Color c = ColorKeeper.GetTheme(theme).colors[colorID];
        c.a = 0;
        img.color = c;

        OnNewLoad(theme);
    }
    Coroutine transitionRoutine;

    Color finalColor;
    IEnumerator Transition()
    {
        float startTime = Time.time;
        Color startColor = img.color;
        while (Time.time <= startTime + firstFadeTime)
        {
            float frac = Mathf.Clamp01((Time.time - startTime) / firstFadeTime);
            img.color = Color.Lerp(startColor, finalColor, frac);
            yield return null;
        }
        img.color = finalColor;
        transitionRoutine = null;
    }

    private void OnEnable()
    {
        StopAllCoroutines();
        if (img) img.color = finalColor;
    }
    //private void OnDisable()
    //{

    //}

    private void OnNewLoad(Theme theme)
    {
        if (img != null)
        {
            finalColor = ColorKeeper.GetTheme(theme).colors[colorID];
            if (this.gameObject.activeInHierarchy && this.enabled)
            {
                if (transitionRoutine != null) StopCoroutine(transitionRoutine);
                transitionRoutine = StartCoroutine(Transition());
            }
        }

    }

    private void OnDestroy()
    {
        LevelPrefabManager.onThemeChanged -= OnNewLoad;
    }
}

