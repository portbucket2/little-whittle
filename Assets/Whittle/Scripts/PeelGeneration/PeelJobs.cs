﻿using Unity.Entities;
using Unity.Collections;
using Unity.Mathematics;
using Unity.Burst;
using Unity.Jobs;
using UnityEngine;
[BurstCompile]
public struct PeelJob0 : IJobChunk
{

    public NativeArray<Vector3> verts;
    public NativeArray<Vector3> norms;
    public NativeArray<Vector2> uvs;
    public NativeArray<Color> colors;

    //public int current_peel_id;
    //public int current_count;
    //public int other_count;
    [ReadOnly] public int wCount;
    [ReadOnly] public int vCountPerFiber;

    //[ReadOnly] public ArchetypeChunkSharedComponentType<Peel_ID> peel_id_type;
    [ReadOnly] public ArchetypeChunkComponentType<Fiber_ID> fiber_id_type;
    [ReadOnly] public ArchetypeChunkBufferType<Fiber_VX> vx_type;
    public void Execute(ArchetypeChunk chunk, int chunkIndex, int firstEntityIndex)
    {
        //NativeArray<Peel_ID> pid_array = chunk.GetNativeArray(peel_id_type);
        NativeArray<Fiber_ID> fid_array = chunk.GetNativeArray(fiber_id_type);
        BufferAccessor<Fiber_VX> vx_buffer_accessor = chunk.GetBufferAccessor(vx_type);

        for (int c = 0; c < chunk.Count; c++)
        {
            DynamicBuffer<Fiber_VX> vx_buffer = vx_buffer_accessor[c];

            int fid = fid_array[c].fiber_index;

            for (int i = 0; i < vx_buffer.Length; i++)
            {
                int vi;
                if (i < wCount)
                {
                    vi = fid * vCountPerFiber + i;
                }
                else
                {
                    vi = fid * vCountPerFiber + vCountPerFiber-i-1+wCount;
                }

                verts[vi] = vx_buffer[i].pos;
                norms[vi] = vx_buffer[i].nor;
                uvs[vi] = vx_buffer[i].uv;
                colors[vi] = new Color(0, 0, 0, vx_buffer[i].depth);

            }
            
        }
    }
}

[BurstCompile]
public struct PeelJob1 : IJob
{

    public NativeArray<int> tris;
    [ReadOnly] public int fiberCount;
    [ReadOnly] public int vCountPerFiber;

    public void Execute()
    {
        int Nv = vCountPerFiber;
        int Nf = fiberCount;


        for (int f = 0; f < Nf-1; f++)
        {

            for (int i = 0; i < Nv; i++)
            {
                if (i == Nv - 1)
                {
                    tris[f * (Nv) * 6 + 6 * i + 0] = (f + 0) * Nv + i + 0;
                    tris[f * (Nv) * 6 + 6 * i + 1] = (f + 1) * Nv + 0;
                    tris[f * (Nv) * 6 + 6 * i + 2] = (f + 0) * Nv + 0;

                    tris[f * (Nv) * 6 + 6 * i + 3] = (f + 0) * Nv + i + 0;
                    tris[f * (Nv) * 6 + 6 * i + 4] = (f + 1) * Nv + i + 0;
                    tris[f * (Nv) * 6 + 6 * i + 5] = (f + 1) * Nv + 0;
                }
                else
                {
                    tris[f * (Nv) * 6 + 6 * i + 0] = (f + 0) * Nv + i + 0;
                    tris[f * (Nv) * 6 + 6 * i + 1] = (f + 1) * Nv + i + 1;
                    tris[f * (Nv) * 6 + 6 * i + 2] = (f + 0) * Nv + i + 1;

                    tris[f * (Nv) * 6 + 6 * i + 3] = (f + 0) * Nv + i + 0;
                    tris[f * (Nv) * 6 + 6 * i + 4] = (f + 1) * Nv + i + 0;
                    tris[f * (Nv) * 6 + 6 * i + 5] = (f + 1) * Nv + i + 1;
                }
            }

        }

    }
}