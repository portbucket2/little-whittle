﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PeelGenerationManager : MonoBehaviour
{
    public static PeelGenerationManager instance;
    public static PeelSettings peelSettings { get { if (instance) return instance.settings; else return null; } }
    public bool showEjectLog = true;
    public PeelSettings settings;

    public PeelBehaviour peelBuilder;

    #region Focus Control
    internal bool focusNow;
    internal Vector3 GetFinalBaseRotationSpeed()
    {
        if (focusNow && false)// ABManager.GetValue(ABtype.FOCUS_SPEED) == 1)
        {
            return settings.baseRotationSpeed * settings.focusSpeedRatio;
        }

        return settings.baseRotationSpeed;
    }
    public static void SetFocus(bool focus)
    {
        if (instance) instance.focusNow = focus;
    }
    #endregion

    public void Awake()
    {
        instance = this;
        PeelBehaviour.Init(Unity.Entities.World.Active.EntityManager);
    }


    public void OnLateUpdate_ManageHit(float dirtRatio)
    {

        Vector3 cutoutCentre = CutOutData.GetCentre();
        if (peelBuilder == null)
        {
            if (dirtRatio > settings.dirtyThreshold_new)
            {
                peelBuilder = PeelBehaviour.Create(settings);// new PeelBuilder(PeelableController.instance, hit, rotationPerTravelUnit, rotationReductionMax, peelDivisionLength, arcVertices);
                peelBuilder.AddFiber();
                peelBuilder.AddFiber();
            }
        }
        else
        {
            float strech = peelBuilder.GetPotentialStrechDistance(cutoutCentre);

            bool angleBreak = false;
            if (peelBuilder.fibers.Count > 2 && strech >= settings.peelDivisionLength)
            {
                if (peelBuilder.GetTravelVec().sqrMagnitude > 0)
                {
                    //Vector3 point = peelBuilder.root.InverseTransformPoint(cutoutCentre);
                    //float angle0 = Vector3.Angle(peelBuilder.GetTravelVec(), peelBuilder.GetTravelVec_1());
                    //float angle1 = Vector3.Angle(peelBuilder.GetTravelVec(), peelBuilder.GetPotentialTravelVec(cutoutCentre));
                    float angle2 = Vector3.Angle(peelBuilder.GetTravelVec_1(), peelBuilder.GetPotentialTravelVec(cutoutCentre));
                    //if (angle1 > settings.angleCutOffLimit)
                    //{
                    //    angleBreak = true;
                    //}
                    //else 
                    if (angle2 > settings.angleCutOffLimit)
                    {
                        angleBreak = true;
                    }
                    //if (angleBreak)
                    //{
                    //    Debug.LogFormat("angles 0{0}, 1: {1}, 2: {2}",angle0, angle1,angle2);
                    //}
                }


            }

            bool fiberCountBreak = peelBuilder.fibers.Count >= PeelBehaviour.MAX_FIBER_COUNT;// settings.fiberCountCutOffLimit;
            bool jumpLimitBreak = strech >= settings.jumpDistanceLimit;

            if (jumpLimitBreak || angleBreak || fiberCountBreak)
            {
                Eject(string.Format("Jump break {0}, angle Break {1}, fiber count Break {2}", jumpLimitBreak, angleBreak, fiberCountBreak), jumpLimitBreak || angleBreak);
                peelBuilder = PeelBehaviour.Create(settings);// new PeelBuilder(PeelableController.instance, hit, rotationPerTravelUnit, rotationReductionMax, peelDivisionLength, arcVertices);
                peelBuilder.AddFiber();
                peelBuilder.AddFiber();
            }
            else if (strech >= settings.peelDivisionLength)
            {
                if (dirtRatio > settings.dirtyThreshold_old)
                {
                    peelBuilder.AddFiber();
                    //peelBuilder.UpdateLastFibers();
                }
                else
                {
                    Eject("Not enough vertices for new fiber", true);
                }
            }
            else
            {
                peelBuilder.UpdateLastFibers();
            }

            if (peelBuilder != null && peelBuilder.GetTotalFiberLength() > nextCutOffLength)
            {
                Eject("Peel too long", false);
            }
        }
        //if (peelBuilder != null) peelBuilder.UpdateFiberMesh();

    }
    public void OnLateUpdate_ManageMiss()
    {
        Eject("No input or miss", false);
    }


    private float nextCutOffLength;
    private void DecideNextCutOffLength()
    {
        nextCutOffLength = Handy.Deviate(settings.lengthCutOffLimit, settings.lengthCutOff_deviation);
    }

    public void Eject(string ejectReason, bool terminate)
    {
        if (peelBuilder == null) return;
        TrashManager.instance.AddPeelToTrash(peelBuilder);
        peelBuilder = null;
        DecideNextCutOffLength();
        if (showEjectLog) Debug.LogFormat("Ejected: {0}", ejectReason);
    }
}
