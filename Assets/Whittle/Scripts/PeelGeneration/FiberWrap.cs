﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;

[System.Serializable]
public class FiberWrap
{
    public int peel_id;
    public int fiber_id;

    public Entity entity;

    public Vector3 basePoint;//peel local space
    public Vector3 upOffset; //peel local space
    public Vector3 outerDir;
    public float lastRecordedLength;
    
    public FiberWrap(PeelBehaviour peelBehaviour, EntityManager eMan, EntityArchetype fiberArchetype, int fiber_id)
    {

        this.fiber_id = fiber_id;
        peel_id = peelBehaviour.peelID;
        lastRecordedLength = 0;

        entity = eMan.CreateEntity(fiberArchetype);
        SetInitialData(peelBehaviour, eMan);

    }
    private void SetInitialData(PeelBehaviour peelBehaviour, EntityManager eMan)
    {

        eMan.SetSharedComponentData(entity, new Peel_ID
        {
            peel_id = this.peel_id
        });

        eMan.SetComponentData(entity, new Fiber_ID
        {
            fiber_index = this.fiber_id
        });

        SetData(peelBehaviour, eMan);
    }
    public void SetData(PeelBehaviour peelBehaviour, EntityManager eMan)
    {
        basePoint = CutOutData.GetCentre(peelBehaviour.root);
        upOffset = peelBehaviour.root.InverseTransformDirection(CutOutData.DirEdge).normalized;
        outerDir = peelBehaviour.root.InverseTransformDirection(-CutOutData.DirDeform).normalized;


        DynamicBuffer<Fiber_VX> vxBuffer = eMan.GetBuffer<Fiber_VX>(entity);
        while (vxBuffer.Length< CutOutData.vCount)
        {
            vxBuffer.Add(new Fiber_VX { });
        }
        for (int i = 0; i < CutOutData.vCount; i++)
        {
            vxBuffer[i] = new Fiber_VX
            {
                pos = CutOutData.GetVPOS(i, peelBehaviour.root),
                nor = CutOutData.GetNORM(i, peelBehaviour.root),
                uv = CutOutData.uv[i],
                depth = CutOutData.depth[i]
            };
        }
    }
}
