﻿using FRIA;
using System.Collections.Generic;
using Unity.Entities;
using UnityEngine;

public class PeelBehaviour : MonoBehaviour
{
    public const int MAX_FIBER_COUNT = 500;
    public const int EDGE_FIBER_COUNT = 5;
    public static int MaxEffectiveFiberCount { get { return MAX_FIBER_COUNT + EDGE_FIBER_COUNT * 2; } }
    public static int peelIDindex = 0;

    public static List<PeelBehaviour> allRegisteredPeels = new List<PeelBehaviour>();

    public static EntityManager eMan;
    public static EntityArchetype fiberArchetype;

    public static void Init(EntityManager eManager)
    {
        eMan = eManager;
        fiberArchetype = eManager.CreateArchetype(
            typeof(Peel_ID),
            typeof(Fiber_ID),
            typeof(Fiber_VX)
        );
    }

    public static PeelBehaviour Create(PeelSettings peelSettings)
    {
        peelIDindex++;
        GameObject gameObject = new GameObject(string.Format("Peel {0}", peelIDindex));
        PeelBehaviour peelBehaviour = gameObject.AddComponent<PeelBehaviour>();
        allRegisteredPeels.Add(peelBehaviour);
        peelBehaviour.Initialize(peelSettings);
        return peelBehaviour;
    }

    //initialized fields
    public Transform root;
    public GameObject rootGo;
    public Rigidbody rgbd;
    public Mesh mesh;
    public int peelID = -1;
    PeelSettings settings;

    MeshFilter filter;
    MeshRenderer rend;

    //running fields
    float lastLength = 0;

    public float ejectTime;
    public float ejectYpos;

    #region easy access functions
    public FiberWrap lastFiber_0
    {
        get
        {
            if (fibers.Count < 1) return null;
            else return fibers[fibers.Count - 1];
        }
    }
    public FiberWrap lastFiber_1
    {
        get
        {
            if (fibers.Count < 2) return null;
            else return fibers[fibers.Count - 2];
        }
    }
    public FiberWrap lastFiber_2
    {
        get
        {
            if (fibers.Count < 3) return null;
            else return fibers[fibers.Count - 3];
        }
    }
    public float GetTotalFiberLength()
    {
        float length = 0;
        for (int i = 0; i < fibers.Count; i++)
        {
            length += fibers[i].lastRecordedLength;
        }
        return length;
    }
    public Vector3 GetTravelVec()
    {
        return lastFiber_0.basePoint - lastFiber_1.basePoint;
    }
    public Vector3 GetTravelVec_1()
    {
        return lastFiber_1.basePoint - lastFiber_2.basePoint;
    }
    public Vector3 GetPotentialTravelVec(Vector3 pointInWorldSpace)
    {
        return  root.InverseTransformPoint(pointInWorldSpace) - lastFiber_1.basePoint;
    }
    public float GetLastInterFiberDistance()
    {
        return GetTravelVec().magnitude;
    }
    public float GetPotentialStrechDistance(Vector3 pointInWorldSpace)
    {
        return (pointInWorldSpace - root.TransformPoint(lastFiber_1.basePoint)).magnitude;
    }
    #endregion

    public int VisualFiberCount { get { return fibers.Count + edgeFibers.Count; } }

    public List<FiberWrap> fibers = new List<FiberWrap>();
    public List<EdgeFiberWrap> edgeFibers = new List<EdgeFiberWrap>();
    public List<Transform> colliders = new List<Transform>();

    private void Initialize(PeelSettings peelSettings)
    {
        peelID = peelIDindex;
        rootGo = this.gameObject;
        root = this.transform;
        settings = peelSettings;


        root.position = CutOutData.GetCentre();
        root.rotation = Quaternion.identity;

        filter = rootGo.AddComponent<MeshFilter>();
        rend = rootGo.AddComponent<MeshRenderer>();
        mesh = new Mesh();
        mesh.name = string.Format("Peel Mesh {0}", peelID);
        filter.mesh = mesh;
        rend.material = settings.skinMaterial;
    }

    

    public void AddFiber()
    {
        FiberWrap fiber = new FiberWrap(this,eMan,fiberArchetype, EDGE_FIBER_COUNT+fibers.Count);
        fibers.Add(fiber);
        lastLength = 0;

        if (fibers.Count % 10 == 3)
        {
            Transform colTr = Pool.Instantiate(settings.colliderPrefab, root).transform;
            colTr.position = root.TransformPoint(lastFiber_2.basePoint);
            colliders.Add(colTr);
        }

        UpdateLastFibers();
    }

    public void UpdateLastFibers()
    {
        lastFiber_0.SetData(this, eMan);
        if (fibers.Count < 2) return;

        Vector3 travelDirection = lastFiber_0.basePoint - lastFiber_1.basePoint;
        lastFiber_0.lastRecordedLength = travelDirection.magnitude;
        if (lastFiber_0.lastRecordedLength == 0) return;

        //this would be a good place if i want to fix orientation of last peel

        float currentLength = GetLastInterFiberDistance();
        float lengthIncrease = currentLength - lastLength;
        lastLength = GetLastInterFiberDistance();


        Vector3 axis = root.TransformDirection(lastFiber_0.upOffset);
        Vector3 point = root.TransformPoint(lastFiber_0.basePoint);
        float rotationAmount = settings.rotationPerTravelUnit - (1 - (1 / Mathf.Pow(GetTotalFiberLength(), 1 / 16))) * settings.rotationReductionMax;
        if (GetTotalFiberLength() > 1)
        {
            rotationAmount -= (1 - (1 / GetTotalFiberLength())) * settings.rotationReductionMax;
        }
        float rotationInUse;
        float calculatedRot = lengthIncrease * rotationAmount / (Time.deltaTime);

        if (lastNormalizedRotation == 0)
            rotationInUse = calculatedRot;
        else
            rotationInUse = Mathf.Lerp(lastNormalizedRotation, calculatedRot, settings.rotLerpRate);

        lastNormalizedRotation = rotationInUse;

        root.RotateAround(point, -axis, rotationInUse*Time.deltaTime);

        if (edgeFibers.Count == 0)
        {
            CreateEdges();
        }
        else
        {
            UpdateEdges();
        }
    }
    float lastNormalizedRotation = 0;

    private void CreateEdges()
    {
        for (int i = 0; i < EDGE_FIBER_COUNT*2; i++)
        {
            EdgeFiberWrap fiber = new EdgeFiberWrap(this, eMan, fiberArchetype);
            edgeFibers.Add(fiber);
        }
        UpdateEdges(true);
    }
    private void UpdateEdges(bool bothSides =false)
    {
        for (int i = 0; i < EDGE_FIBER_COUNT; i++)
        {
            float f = i / ((float)EDGE_FIBER_COUNT);
            float offsetFrac = 1 - f;
            float scaleFrac;
            if (f < 0.5f)
            {
                scaleFrac = Mathf.Pow(2 * f, 3) / 2;
            }
            else
            {
                scaleFrac = (Mathf.Pow(2 * f - 1, 0.5f) + 1) / 2;
            }
            if (scaleFrac == 0) scaleFrac = 0.01f;
            if(bothSides)
            {
                EdgeFiberWrap startSide = edgeFibers[i];
                startSide.SetData(i,this,eMan,-offsetFrac*settings.maxEdgeOffset,scaleFrac);
            }
            EdgeFiberWrap endSide = edgeFibers[edgeFibers.Count-1-i];
            endSide.SetData(fibers.Count+EDGE_FIBER_COUNT+EDGE_FIBER_COUNT-1-i, this, eMan, offsetFrac * settings.maxEdgeOffset, scaleFrac);
        }
    }

    public float EjectPeel(bool disablePhysics, float ejectForce)
    {
        ejectYpos = root.position.y;
        ejectTime = Time.time;

        if (!disablePhysics)
        {
            rgbd = root.gameObject.AddComponent<Rigidbody>();
            rgbd.AddForce(root.TransformDirection(lastFiber_0.outerDir) * ejectForce, ForceMode.Impulse);

            if (fibers.Count < 10)
            {
                UnloadPoolColliders();
            }
            //GameObject go = root.gameObject;
            //FRIA.Centralizer.Add_DelayedAct(() => { GameObject.DestroyImmediate(go); }, 1);
        }
        return GetTotalFiberLength();

    }
    public void UnloadPoolColliders()
    {
        for (int i = colliders.Count - 1; i >= 0; i--)
        {
            if (colliders[i]) Pool.Destroy(colliders[i].gameObject, true);
            colliders.RemoveAt(i);
        }

    }
    public void TrashItems()
    {

        //Debug.Log(peelID);
        UnloadPoolColliders();
        GameObject.DestroyImmediate(root.gameObject);
        Mesh.Destroy(mesh);
    }
}
