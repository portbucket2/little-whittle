﻿using UnityEngine;
using Unity.Entities;
using Unity.Collections;
using Unity.Mathematics;
using Unity.Jobs;
using Unity.Burst;

[UpdateBefore(typeof(System0_MainMeshMod))]
public class PeelMeshGeneratorSystem : JobComponentSystem
{
    EntityQuery basicQuery;
    ArchetypeChunkComponentType<Fiber_ID> fid_ro_type;
    ArchetypeChunkBufferType<Fiber_VX> vert_ro_type;



    protected override void OnCreate()
    {
        base.OnCreate();
        basicQuery = this.GetEntityQuery
            (
            typeof(Peel_ID),
            typeof(Fiber_ID),
            typeof(Fiber_VX)
            );

        int fiberCount = PeelBehaviour.MaxEffectiveFiberCount;
        int ARC_N = CutOutData.vCount;

        int count_v= fiberCount * ARC_N;
        int count_t = (fiberCount - 1) * (ARC_N) * 6;
        vert_pos = new NativeArray<Vector3>(count_v,Allocator.Persistent);
        vert_nor = new NativeArray<Vector3>(count_v, Allocator.Persistent);
        vert_uv = new NativeArray<Vector2>(count_v, Allocator.Persistent);
        vert_color = new NativeArray<Color>(count_v, Allocator.Persistent);
        triangles = new NativeArray<int>(count_t, Allocator.Persistent);
        triangles_simple = new int[triangles.Length];



    }
    protected override void OnDestroy()
    {
        base.OnDestroy();
        vert_pos.Dispose();
        vert_nor.Dispose();
        vert_uv.Dispose();
        vert_color.Dispose();
        triangles.Dispose();

    }
    
    private static NativeArray<Vector3> vert_pos;
    private static NativeArray<Vector3> vert_nor;
    private static NativeArray<Vector2> vert_uv;
    private static NativeArray<Color> vert_color;
    private static NativeArray<int> triangles;
    private static int[] triangles_simple;



    //public static int peel_work_scheduler_id = -1;
    //public static void LoadMeshData(PeelBehaviour peelBehaviour)
    //{
    //    if (peel_work_scheduler_id == peelBehaviour.peelID)
    //    {
    //        //pb.mesh.SetVertices(vert_pos);
    //    }
    //}
   
  
    System.Action onJobMustComplete;
    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        fid_ro_type = this.GetArchetypeChunkComponentType<Fiber_ID>(true);
        vert_ro_type = this.GetArchetypeChunkBufferType<Fiber_VX>(true);


        JobHandle previousDependencies = inputDeps;

        onJobMustComplete?.Invoke();

        if (PeelGenerationManager.instance)
        {
            PeelBehaviour pb = PeelGenerationManager.instance.peelBuilder;
            if (pb)
            {
                JobHandle overallHandle;

                basicQuery.SetFilter(new Peel_ID {peel_id = pb.peelID});

                PeelJob0 pj0 = new PeelJob0
                {
                    verts = vert_pos,
                    norms = vert_nor,
                    uvs = vert_uv,
                    colors = vert_color,
                    wCount = CutOutData.N_w,
                    vCountPerFiber = CutOutData.vCount,
                    fiber_id_type = fid_ro_type,
                    vx_type = vert_ro_type
                };
                JobHandle jh0 = pj0.Schedule(basicQuery, previousDependencies);

                PeelJob1 pj1 = new PeelJob1
                {
                    tris = triangles,
                    fiberCount = pb.VisualFiberCount,
                    vCountPerFiber = CutOutData.vCount
                };


                JobHandle jh1 = pj1.Schedule(previousDependencies);
                overallHandle = JobHandle.CombineDependencies(jh0, jh1);

                previousDependencies = overallHandle;


                int total_vert_count = CutOutData.vCount * pb.VisualFiberCount;
                int total_tris_count = (pb.VisualFiberCount - 1) * CutOutData.vCount * 6;
                onJobMustComplete = () =>
                {
                    overallHandle.Complete();
                    if (pb != null)
                    {
                        triangles.CopyTo(triangles_simple);

                        pb.mesh.SetVertices(vert_pos, 0, total_vert_count);
                        pb.mesh.SetNormals(vert_nor, 0, total_vert_count);
                        pb.mesh.SetUVs(0,vert_uv, 0, total_vert_count);
                        pb.mesh.SetColors(vert_color, 0, total_vert_count);
                        pb.mesh.SetTriangles(triangles_simple, 0, total_tris_count, 0);
                    }
                    onJobMustComplete = null;
                };
            }
        }
        return previousDependencies;
    }
}


