﻿using Unity.Collections;
using Unity.Mathematics;
using UnityEngine;

public static class CutOutData
{
    public const int N_w = 7;
    public const int vCount = 14;

    public static NativeArray<float3> interactionDirs;// calc interaction dir output - Dirs: 0 = input, 1 = travel, 2 = avg deform(ini pos- hp), 3= cross(1,2) 
    public static Vector3 DirRay { get { return root.TransformDirection(interactionDirs[0]); } }
    public static Vector3 DirTravel { get { return root.TransformDirection(interactionDirs[1]); } }
    public static Vector3 DirDeform { get { return root.TransformDirection(interactionDirs[2]); } }
    public static Vector3 DirEdge { get { return root.TransformDirection(interactionDirs[3]); } }


    public static NativeArray<float3> vpos_obj;
    public static NativeArray<float3> norm_obj;
    public static NativeArray<float2> uv;
    public static NativeArray<float> depth;
    public static NativeArray<int> assigned_flag;

    public static Transform root;

    public static Vector3 GetVPOS(int index, Transform relativeTrans=null)
    {
        if (index < 0 || index >= vCount) throw new System.Exception("Invalid index!");

        Vector3 v3 = root.TransformPoint(vpos_obj[index]);
        if (relativeTrans)
            return relativeTrans.InverseTransformPoint(v3);
        else return v3;
    }
    public static Vector3 GetNORM(int index, Transform relativeTrans=null)
    {
        if (index < 0 || index >= vCount) throw new System.Exception("Invalid index!");

        Vector3 v3 = root.TransformDirection(norm_obj[index]);
        if (relativeTrans)
            return relativeTrans.InverseTransformDirection(v3);
        else return v3;
    }

    public static Vector3 GetCentre(Transform relativeTrans = null)
    {
        return (GetVPOS(N_w / 2, relativeTrans) + GetVPOS(N_w + (N_w / 2), relativeTrans))/2;
    }
    public static void Init(Transform root)
    {
        CutOutData.root = root;


        interactionDirs = new NativeArray<float3>(4, Allocator.Persistent);

        assigned_flag = new NativeArray<int>(N_w*2, Allocator.Persistent);

        vpos_obj = new NativeArray<float3>(N_w*3, Allocator.Persistent);
        norm_obj = new NativeArray<float3>(N_w*2, Allocator.Persistent);
        uv = new NativeArray<float2>(N_w*2, Allocator.Persistent);
        depth = new NativeArray<float>(N_w*2, Allocator.Persistent);
    }
    public static void Dispose()
    {
        interactionDirs.Dispose();

        assigned_flag.Dispose();
        vpos_obj.Dispose();
        norm_obj.Dispose();
        uv.Dispose();
        depth.Dispose();
        root = null;
    }

}
