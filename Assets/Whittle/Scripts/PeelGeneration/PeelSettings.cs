﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//PeelableController

[CreateAssetMenu(fileName = "Peel Settings", menuName = "Peel Settings")]
public class PeelSettings : ScriptableObject
{

    public GameObject colliderPrefab;
    public Material skinMaterial;
    public BrushValues defaultBrush;

    [Header("Peel Characterstics")]
    public float peelDivisionLength = 0.1f;
    public float rotationPerTravelUnit = -500;
    public float rotationReductionMax = -240;
    public float extrusion = 0.002f;
    [Range(0, 1)]
    public float rotLerpRate = 0.35f;
    public float maxEdgeOffset = 0.03f;
    [Header("Cut Off Limits")]
    //public int fiberCountCutOffLimit = 1000;
    public float lengthCutOffLimit = 10000;
    public float lengthCutOff_deviation = 0.25f;
    public float angleCutOffLimit = 45;
    public float dirtyThreshold_new = 0.2f;
    public float dirtyThreshold_old = 0.01f;
    public float jumpDistanceLimit = 0.500f;

    //[Header("Smoothness")]
    //[SerializeField] float widthSmoothingLerpRate = 20;
    //[SerializeField] float positionSmoothingLerpRate = 10f;

    [Header("Focus")]
    [Range(0, 1)]
    public float focusSpeedRatio = 1;
    public Vector3 baseRotationSpeed = new Vector3(0, 135, 0);


}