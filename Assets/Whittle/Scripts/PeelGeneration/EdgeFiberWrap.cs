﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;

[System.Serializable]
public class EdgeFiberWrap
{
    public int peel_id;
    public int fiber_id;

    public Entity entity;

    public Vector3 basePoint;//peel local space

    
    public EdgeFiberWrap(PeelBehaviour peelBehaviour, EntityManager eMan, EntityArchetype fiberArchetype)
    {
        
        peel_id = peelBehaviour.peelID;

        entity = eMan.CreateEntity(fiberArchetype);

        eMan.SetSharedComponentData(entity, new Peel_ID
        {
            peel_id = this.peel_id
        });

    }
    public void SetData(int fiber_id,PeelBehaviour peelBehaviour, EntityManager eMan, float distance, float scaleDownFactor)
    {

        this.fiber_id = fiber_id;
        eMan.SetComponentData(entity, new Fiber_ID
        {
            fiber_index = this.fiber_id
        });


        Vector3 travelDir = peelBehaviour.root.InverseTransformDirection(CutOutData.DirTravel).normalized;

        basePoint = CutOutData.GetCentre(peelBehaviour.root) + travelDir*distance;


        DynamicBuffer<Fiber_VX> vxBuffer = eMan.GetBuffer<Fiber_VX>(entity);
        while (vxBuffer.Length < CutOutData.vCount)
        {
            vxBuffer.Add(new Fiber_VX { });
        }
        for (int i = 0; i < CutOutData.vCount; i++)
        {
            Vector3 vpos = CutOutData.GetVPOS(i, peelBehaviour.root) + travelDir*distance;
            vpos = (vpos - basePoint) * scaleDownFactor + basePoint;

            vxBuffer[i] = new Fiber_VX
            {
                pos = vpos,
                nor = CutOutData.GetNORM(i, peelBehaviour.root),
                uv = CutOutData.uv[i],
                depth = CutOutData.depth[i]
            };
        }
    }
}
