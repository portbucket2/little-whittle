﻿using Unity.Entities;
using Unity.Mathematics;


public struct Peel_ID : ISharedComponentData
{
    public int peel_id;
}
public struct Fiber_ID : IComponentData
{
    public int fiber_index;
}

[InternalBufferCapacity(10)]
public struct Fiber_VX : IBufferElementData
{
    public float3 pos;
    public float3 nor;
    public float2 uv;
    public float depth;
}

//[InternalBufferCapacity(10)]
//public struct Fiber_NR : IBufferElementData
//{
//    public float3 value;
//}

//[InternalBufferCapacity(10)]
//public struct Fiber_UV : IBufferElementData
//{
//    public float2 value;

//}
//[InternalBufferCapacity(10)]
//public struct Fiber_DP : IBufferElementData
//{
//    public float value;
//}