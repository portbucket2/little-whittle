﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class BrushValues 
{
    [Header("Brush Data")]
    [Range(0.01f, 0.20f)]
    public float brushRadius = 0.181f;
    [Range(0, 0.5f)]
    public float brushStrength = 0.4f;
}
