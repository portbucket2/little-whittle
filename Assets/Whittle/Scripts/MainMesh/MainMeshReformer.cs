﻿using Unity.Collections;
using Unity.Jobs;
using Unity.Mathematics;
using UnityEngine;
public class MainMeshReformer : MonoBehaviour
{

    public static MainMeshReformer instance;
    public static Transform meshTrans;
    public MeshFilter meshFilterIn;
    public MeshFilter meshFilterOut;
    [HideInInspector] public Mesh mesh;
    [HideInInspector] public Transform hitPointer;
    [HideInInspector] public Transform vertPointer;

    public JobHandle VertModJob;

    public NativeArray<Vector3> vposNativeArray;    //copy job output
    public NativeArray<Vector3> normNativeArray;    //copy job output
    public NativeArray<Color> vcolNativeArray;      //copy job output



    public NativeArray<float> hitDepthAtFrame;  // find hit job output - depth of hit and whetther hit has occured
    public NativeArray<float3> hitPointAtFrame; // find hit job output - centre of deforming sphere
    public NativeArray<float3> hitVertData;     // find hit job output - first vertex to hit
    public NativeArray<int> intData;            //[0] find hit job output - hit vert was completed , [1] calc avg deform job output - number of verts deformed

    //public NativeArray<float3> interactionDirs; // calc interaction dir output - Dirs: 0 = input, 1 = travel, 2 = avg deform(ini pos- hp), 3= cross(1,2) 
    public NativeArray<float3> bboxData;        // calc bounds output - data of 2d bounding box of cut mesh



    public NativeArray<float> floatData; //[0] bbox height, [1] bbox width, [2] dirtyVerts handled, [3] total verts handled, [4] overall progress,[5] max progress
    //public NativeArray<float3> cutOut_vpos; //3x of count
    //public NativeArray<int> cutOutPoints_assignmentflag; //3x of count





    private void Awake()
    {
        //Debug.Log("MMR A");
        instance = this;
        CutOutData.Init(meshFilterOut.transform);

    }

    [System.NonSerialized]
    public BrushValues currentBrush;

    //[Range(0,1)]
    [HideInInspector] public float totalProgress;
    //[HideInInspector] public float maxProgress;
    public void Init(int VertCount, Mesh mesh)
    {
        CustomBrushValues customBrushMono = this.GetComponent<CustomBrushValues>();
        if (customBrushMono)
        {
            currentBrush = customBrushMono.customBrush;
        }
        else
        {
            currentBrush = PeelGenerationManager.peelSettings.defaultBrush;
        }
        //Debug.Log("MMR I");
        meshFilterIn.gameObject.SetActive(false);
        meshFilterOut.mesh = mesh;
        meshFilterOut.GetComponent<MeshRenderer>().material = PeelGenerationManager.peelSettings.skinMaterial;
        this.mesh = mesh;

        meshTrans = instance.meshFilterOut.transform;


        vposNativeArray = new NativeArray<Vector3>(VertCount, Allocator.Persistent);
        normNativeArray = new NativeArray<Vector3>(VertCount, Allocator.Persistent);
        vcolNativeArray = new NativeArray<Color>(VertCount, Allocator.Persistent);

        hitDepthAtFrame = new NativeArray<float>(2, Allocator.Persistent);
        hitPointAtFrame = new NativeArray<float3>(2, Allocator.Persistent);
        hitVertData = new NativeArray<float3>(2, Allocator.Persistent);
        intData = new NativeArray<int>(2,Allocator.Persistent);


        bboxData = new NativeArray<float3>(4, Allocator.Persistent);


        floatData = new NativeArray<float>(6, Allocator.Persistent);
        //floatData[4] = 0;
        totalProgress = 0;
        //maxProgress = mesh.vertices.Length;
        //cutOut_vpos = new NativeArray<float3>(CUTOUT_WIDTH_COUNT*3, Allocator.Persistent);
        //cutOutPoints_assignmentflag = new NativeArray<int>(CUTOUT_WIDTH_COUNT * 2, Allocator.Persistent);


        intData[0] = 1;
        intData[1] = 0;
        hitDepthAtFrame[0] = float.MaxValue;
        hitPointAtFrame[0] = new float3(0, 0, 0);
        hitDepthAtFrame[1] = float.MaxValue;
        hitPointAtFrame[1] = new float3(0, 0, 0);

        LevelPrefabManager.onUnload += OnUnload;
    }
    private void OnUnload()
    {
        LevelPrefabManager.onUnload -= OnUnload;

        //Debug.Log("MMR D");
        vposNativeArray.Dispose();
        normNativeArray.Dispose();
        vcolNativeArray.Dispose();

        hitDepthAtFrame.Dispose();
        hitPointAtFrame.Dispose();
        hitVertData.Dispose();
        intData.Dispose();

        bboxData.Dispose();

        floatData.Dispose();
        //cutOut_vpos.Dispose();
        //cutOutPoints_assignmentflag.Dispose();
        CutOutData.Dispose();
    }


    int frameCount;
    private void LateUpdate()
    {

        VertModJob.Complete();

        if (InputRayManager.inputRay != null)
        {
            totalProgress = floatData[4] / floatData[5];
            //Debug.LogFormat("{0}/{1}", floatData[4], floatData[5]);
            LevelPrefabManager.currentLevel.ReportProgress(totalProgress);
        }
        int frameOffset = frameCount++ % 3;
        switch (frameOffset)
        {
            case 0:
                mesh.SetVertices(vposNativeArray);
                break;
            case 1:
                mesh.SetNormals(normNativeArray);
                break;
            case 2:
                mesh.SetColors(vcolNativeArray);
                break;
        }
        //mesh.SetVertices(vposNativeArray);
        //mesh.SetNormals(normNativeArray);
        //mesh.SetColors(vcolNativeArray);

        if(hitPointer)hitPointer.gameObject.SetActive(hitDepthAtFrame[0] != float.MaxValue);
        if(vertPointer)vertPointer.gameObject.SetActive(hitDepthAtFrame[0] != float.MaxValue);
        if (hitDepthAtFrame[0] != float.MaxValue)
            PeelGenerationManager.instance.OnLateUpdate_ManageHit(dirtRatio: floatData[2] / floatData[3]);
        else
            PeelGenerationManager.instance.OnLateUpdate_ManageMiss();

        if (hitDepthAtFrame[0] != float.MaxValue)
        {
            if (vertPointer) vertPointer.position = meshFilterOut.transform.TransformPoint(hitVertData[0]);
            if (hitPointer) hitPointer.position = meshFilterOut.transform.TransformPoint(hitPointAtFrame[0]);
            if (hitPointer) hitPointer.localScale = Vector3.one * MainMeshReformer.instance.currentBrush.brushRadius * 2;


#if UNITY_EDITOR
            //Debug.DrawRay(hitPointer.position, meshFilter.transform.TransformDirection(interactionDirs[2]) * 1, Color.red);
            //Debug.DrawRay(hitPointer.position, meshFilter.transform.TransformDirection(interactionDirs[3]) * 1, Color.magenta);
            //Debug.DrawRay(hitPointer.position, meshFilter.transform.TransformDirection(CutOutData.interactionDirs[1]) * 0.2f, Color.green);
            DrawDirBox(Color.green);
            DrawBBox(Color.red);
            Vector3 bboxheight = bboxData[1] - bboxData[0];
            //Vector3 bboxwidth = bboxData[3] - bboxData[2];
            //Debug.LogFormat("height {0}, width {1}", bboxheight.magnitude, bboxwidth.magnitude);
            for (int i = 0; i < CutOutData.N_w; i++)
            {
                Debug.DrawLine(CutOutData.GetVPOS(i), CutOutData.GetVPOS(i + CutOutData.N_w), Color.cyan);

                Vector3 dif = CutOutData.vpos_obj[i] - CutOutData.vpos_obj[i + CutOutData.N_w];
                if ((dif.magnitude / bboxheight.magnitude) > 1.05f)
                {
                    Debug.LogErrorFormat("Big By Ratio <{0}> at index- {1}", dif.magnitude / bboxheight.magnitude, i);
                    ReportFlagCondition();
                }

            }

#endif
        }

        PostLateUpdateReset();

    }
    private void ReportFlagCondition()
    {
        string strT = "TOP: ";
        //string strB = "BOT: ";
        for (int j = 0; j < CutOutData.N_w; j++)
        {
            strT += string.Format("({0} {1}) ", CutOutData.assigned_flag[j], CutOutData.assigned_flag[j + CutOutData.N_w]);
            //strB += string.Format("{0}_", );
        }
        Debug.Log(strT);
        //Debug.Log(strB);
    }
    private void PostLateUpdateReset()
    {
        intData[0] = 1; //default of vert hit completion is set to 1/true;
        intData[1] = 0; //default of deformed vert count is set to 0.

        hitDepthAtFrame[1] = hitDepthAtFrame[0];
        hitPointAtFrame[1] = hitPointAtFrame[0];

        hitDepthAtFrame[0] = float.MaxValue;
        hitPointAtFrame[0] = new float3(0, 0, 0);
    }

    public void DrawDirBox(Color col)
    {
        float x = .15f;
        float y = .075f;
        float3 a = meshFilterOut.transform.TransformPoint(hitPointAtFrame[0] + x * CutOutData.interactionDirs[3]);
        float3 b = meshFilterOut.transform.TransformPoint(hitPointAtFrame[0] - x * CutOutData.interactionDirs[3]);
        float3 c = meshFilterOut.transform.TransformPoint(hitPointAtFrame[0] - x * CutOutData.interactionDirs[3] - y * CutOutData.interactionDirs[2]);
        float3 d = meshFilterOut.transform.TransformPoint(hitPointAtFrame[0] + x * CutOutData.interactionDirs[3] - y * CutOutData.interactionDirs[2]);

        Debug.DrawLine(a, b, col);
        Debug.DrawLine(b, c, col);
        Debug.DrawLine(c, d, col);
        Debug.DrawLine(d, a, col);
    }
    public void DrawBBox(Color col)
    {
        //float x = .15f;
        //float y = .075f;
        float3 a = meshFilterOut.transform.TransformPoint(bboxData[0]);
        float3 b = meshFilterOut.transform.TransformPoint(bboxData[2]);
        float3 c = meshFilterOut.transform.TransformPoint(bboxData[1]);
        float3 d = meshFilterOut.transform.TransformPoint(bboxData[3]);

        Debug.DrawLine(a, b, col);
        Debug.DrawLine(b, c, col);
        Debug.DrawLine(c, d, col);
        Debug.DrawLine(d, a, col);
    }
}
