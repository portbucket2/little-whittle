﻿using UnityEngine;
using Unity.Entities;
using Unity.Collections;
using Unity.Mathematics;
using Unity.Jobs;
using Unity.Burst;

[UpdateInGroup(typeof(SimulationSystemGroup))]
public class System0_MainMeshMod : JobComponentSystem
{
    private const int MAX_CHUNK_COUNT = 500;

    EntityQuery basicQuery;
    EntityQuery basicQuery2;
    ArchetypeChunkComponentType<VertexPosition> vpos_rw;
    ArchetypeChunkComponentType<VertexNormal> norm_rw;
    ArchetypeChunkComponentType<VertexDeform> dfrm_rw;
    ArchetypeChunkComponentType<VertexPosition> vpos_ro;
    ArchetypeChunkComponentType<VertexNormal> norm_ro;
    ArchetypeChunkComponentType<VertexDeform> dfrm_ro;



    NativeArray<int> chunkItemCount;
    NativeArray<float> chunkProgress;

    protected override void OnCreate()
    {
        base.OnCreate();
        basicQuery = this.GetEntityQuery(ComponentType.ReadOnly<VertexPosition>());
        basicQuery2 = this.GetEntityQuery(ComponentType.ReadWrite<VertexDeform>());

        chunkItemCount = new NativeArray<int>(MAX_CHUNK_COUNT,Allocator.Persistent);
        chunkProgress = new NativeArray<float>(MAX_CHUNK_COUNT, Allocator.Persistent);
    }
    protected override void OnDestroy()
    {
        base.OnDestroy();
        chunkItemCount.Dispose();
        chunkProgress.Dispose();
    }

    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        vpos_rw = this.GetArchetypeChunkComponentType<VertexPosition>(false);
        norm_rw = this.GetArchetypeChunkComponentType<VertexNormal>(false);
        dfrm_rw = this.GetArchetypeChunkComponentType<VertexDeform>(false);
        vpos_ro = this.GetArchetypeChunkComponentType<VertexPosition>(true);
        norm_ro = this.GetArchetypeChunkComponentType<VertexNormal>(true);
        dfrm_ro = this.GetArchetypeChunkComponentType<VertexDeform>(true);


        //NativeArray<VertexDeform> defArr = basicQuery2.ToComponentDataArray<VertexDeform>(Allocator.TempJob);
        InputRay iray = InputRayManager.inputRay;
        JobHandle previousDependencies = inputDeps;
        if (iray!=null)
        {
            //Debug.Log("scheduled==================================");
            //EntityManager em = MeshDataEntitySpwner.entityManager;

            Job0_HitPoint hpJob = new Job0_HitPoint
            {
                rayOrigin = iray.origin,
                rayDirection = iray.direction,
                camDirection = iray.camDirection,
                detectionRadius = MainMeshReformer.instance.currentBrush.brushRadius,
                hitDepthAtFrame = MainMeshReformer.instance.hitDepthAtFrame,
                hitPointAtFrame = MainMeshReformer.instance.hitPointAtFrame,
                hitVertData = MainMeshReformer.instance.hitVertData,
                hitOnCompletedVert = MainMeshReformer.instance.intData,
                vposType = vpos_ro,
                vnorType = norm_ro,
                progType = dfrm_ro
            };
            previousDependencies = hpJob.Schedule(basicQuery,previousDependencies);


            Job1_AvgDeformPoints avgDefJob = new Job1_AvgDeformPoints
            {
                rayDirection = iray.direction,
                radius = MainMeshReformer.instance.currentBrush.brushRadius + MainMeshReformer.instance.currentBrush.brushStrength * Time.deltaTime,
                hitDepthAtFrame = MainMeshReformer.instance.hitDepthAtFrame,
                hitPointAtFrame = MainMeshReformer.instance.hitPointAtFrame,
                vposType = vpos_rw,
                progType = dfrm_rw,
                intData = MainMeshReformer.instance.intData,
                interactionDir = CutOutData.interactionDirs
            };

            previousDependencies = avgDefJob.Schedule(basicQuery, previousDependencies);


            Job2_InteractionDir sidJob = new Job2_InteractionDir
            {
                rayDirection = iray.direction,
                hitDepthAtFrame = MainMeshReformer.instance.hitDepthAtFrame,
                hitPointAtFrame = MainMeshReformer.instance.hitPointAtFrame,
                hitVertAtFrame = MainMeshReformer.instance.hitVertData,
                interactionDirs = CutOutData.interactionDirs,
                bboxData = MainMeshReformer.instance.bboxData,
                defaultTravelDir = iray.defaultTravelDir
            };

            previousDependencies = sidJob.Schedule(previousDependencies);


            Job3_DeformBounds bbJob = new Job3_DeformBounds
            {
                radius = MainMeshReformer.instance.currentBrush.brushRadius + MainMeshReformer.instance.currentBrush.brushStrength * Time.deltaTime,
                rayDirection = iray.direction,
                hitDepthAtFrame = MainMeshReformer.instance.hitDepthAtFrame,
                hitPointAtFrame = MainMeshReformer.instance.hitPointAtFrame,
                interactionRayData = CutOutData.interactionDirs,
                bboxData = MainMeshReformer.instance.bboxData,
                vposType = vpos_rw,
                progType = dfrm_rw
            };

            previousDependencies = bbJob.Schedule(basicQuery, previousDependencies);


            Job4_PreDeform preDfJob = new Job4_PreDeform
            {
                wCount = CutOutData.N_w,
                hitDepthAtFrame = MainMeshReformer.instance.hitDepthAtFrame,
                bboxData = MainMeshReformer.instance.bboxData,
                floatData = MainMeshReformer.instance.floatData,
                cutoutData = CutOutData.vpos_obj,
                cutoutData_assignment = CutOutData.assigned_flag,
                chunkItemCount = chunkItemCount,
                chunkProgress = chunkProgress
            };

            previousDependencies = preDfJob.Schedule(previousDependencies);


            Job5_Deform dfJob = new Job5_Deform
            {
                radius = MainMeshReformer.instance.currentBrush.brushRadius + MainMeshReformer.instance.currentBrush.brushStrength * Time.deltaTime,
                rayDirection = iray.direction,
                hitDepthAtFrame = MainMeshReformer.instance.hitDepthAtFrame,
                hitPointAtFrame = MainMeshReformer.instance.hitPointAtFrame,
                interactionDirs = CutOutData.interactionDirs,
                wCount = CutOutData.N_w,
                floatData = MainMeshReformer.instance.floatData,
                vposType = vpos_rw,
                vnorType = norm_rw,
                progType = dfrm_rw,
                cutOutPoints = CutOutData.vpos_obj,
                cutOutPoints_nor = CutOutData.norm_obj,
                cutOutPoints_uv = CutOutData.uv,
                cutOutPoints_depth = CutOutData.depth,
                cutoutData_assignment = CutOutData.assigned_flag,
                chunkItemCount = chunkItemCount,
                chunkProgress = chunkProgress
            };

            previousDependencies = dfJob.Schedule(basicQuery, previousDependencies);



            Job6_PostDeform pstDfJob = new Job6_PostDeform
            {
                wCount = CutOutData.N_w,
                hitDepthAtFrame = MainMeshReformer.instance.hitDepthAtFrame,
                interactionDirs = CutOutData.interactionDirs,
                cutoutData = CutOutData.vpos_obj,
                cutOutPoints_nor = CutOutData.norm_obj,
                cutOutPoints_uv = CutOutData.uv,
                cutOutPoints_depth = CutOutData.depth,
                cutoutData_assignment = CutOutData.assigned_flag
            };

            previousDependencies = pstDfJob.Schedule(previousDependencies);


            Job7_ProgressCalc prgJob = new Job7_ProgressCalc
            {
                floatData = MainMeshReformer.instance.floatData,
                chunkItemCount = chunkItemCount,
                chunkProgress = chunkProgress
            };
            previousDependencies = prgJob.Schedule(previousDependencies);
        }



        CopyJob cj = new CopyJob
        {
            posA = MainMeshReformer.instance.vposNativeArray,
            norA = MainMeshReformer.instance.normNativeArray,
            colA = MainMeshReformer.instance.vcolNativeArray
        };
        previousDependencies = cj.Schedule(this, previousDependencies);

        MainMeshReformer.instance.VertModJob = previousDependencies;
        return previousDependencies;
    }
}


