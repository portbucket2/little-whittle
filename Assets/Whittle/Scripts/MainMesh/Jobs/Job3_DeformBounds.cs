﻿using Unity.Entities;
using Unity.Collections;
using Unity.Mathematics;
using Unity.Burst;
using UnityEngine;

[BurstCompile]
public struct Job3_DeformBounds : IJobChunk
{
    /// <summary>
    /// O = centre of hit impact sphere
    /// C = current position of vertex
    /// P = point on sphere where vertex will travel to
    /// </summary>
    [ReadOnly] public float3 rayDirection;
    [ReadOnly] public float radius;
    [ReadOnly] public NativeArray<float> hitDepthAtFrame;
    [ReadOnly] public NativeArray<float3> hitPointAtFrame;
    [ReadOnly] public NativeArray<float3> interactionRayData;

    public NativeArray<float3> bboxData;

    [ReadOnly] public ArchetypeChunkComponentType<VertexPosition> vposType;
    [ReadOnly] public ArchetypeChunkComponentType<VertexDeform> progType;
    public void Execute(ArchetypeChunk chunk, int chunkIndex, int firstEntityIndex)
    {
        NativeArray<VertexPosition> vposArray = chunk.GetNativeArray(vposType);
        NativeArray<VertexDeform> progArray = chunk.GetNativeArray(progType);


        //Debug.LogFormat("{0} _ {1} _ {2}",vposArray.Length,vnorArray.Length,progArray.Length);
        for (int i = 0; i < vposArray.Length; i++)
        {
            VertexPosition vpos = vposArray[i];
            VertexDeform defProg = progArray[i];
            if (hitDepthAtFrame[0] == float.MaxValue)
            {
                return;
            }
            else
            {
                float3 pos_O = hitPointAtFrame[0];
                float3 vec_V_O = pos_O - vpos.current;
                float dist_V_O = math.sqrt(vec_V_O.x * vec_V_O.x + vec_V_O.y * vec_V_O.y + vec_V_O.z * vec_V_O.z);
                float distRatio = dist_V_O / radius;

                if (distRatio < 1)
                {
                    float3 vec_I_F = vpos.final - vpos.initial;
                    float3 vec_C_F = vpos.final - vpos.current;
                    float dot_CF_IF = math.dot(vec_C_F, vec_I_F);
                    if (dot_CF_IF < 0)
                    {
                        vpos.current = vpos.final;
                        UpdateBBoxIfNecessaryForValue(vpos.current);

                    }
                    else if (dot_CF_IF > 0)
                    {
                        float3 deform_dir = vec_I_F / defProg.mag_I_F;

                        float t1 = math.dot(vec_V_O, deform_dir);
                        float distance_from_dir_sqr = dist_V_O * dist_V_O - t1 * t1;
                        float t2 = math.sqrt(radius * radius - distance_from_dir_sqr);

                        float travelDist = t1 + t2;


                        float sqr_mag_C_F = vec_C_F.x * vec_C_F.x + vec_C_F.y * vec_C_F.y + vec_C_F.z * vec_C_F.z;
                        if (sqr_mag_C_F < travelDist * travelDist)
                        {
                            vpos.current = vpos.current + vec_C_F;
                            travelDist = math.sqrt(sqr_mag_C_F);
                        }
                        else
                        {
                            vpos.current = vpos.current + (deform_dir * travelDist);
                        }

                        UpdateBBoxIfNecessaryForValue(vposArray[i].current);
                        UpdateBBoxIfNecessaryForValue(vpos.current);
                    }
                    else
                    {
                        UpdateBBoxIfNecessaryForValue(vpos.current);
                    }
                }               
            }




        }


    }
    void UpdateBBoxIfNecessaryForValue(float3 pos_P)
    {
        float3 pos_O = hitPointAtFrame[0];
        //Vector3 vec_OP = pos_P - pos_O;

        float3 dirA = interactionRayData[2];
        float3 dirB = interactionRayData[3];

        float dotA = math.dot(dirA, pos_P - pos_O);
        float dotB = math.dot(dirB, pos_P - pos_O);

        float dotA_min = math.dot(dirA, bboxData[0] - pos_O);
        float dotA_max = math.dot(dirA, bboxData[1] - pos_O);
        float dotB_min = math.dot(dirB, bboxData[2] - pos_O);
        float dotB_max = math.dot(dirB, bboxData[3] - pos_O);
        if (dotA <= dotA_min)
        {
            bboxData[0] = pos_O + dotA * dirA;
        }

        if (dotA >= dotA_max)
        {
            bboxData[1] = pos_O + dotA * dirA;
        }

        if (dotB <= dotB_min)
        {
            bboxData[2] = pos_O + dotB * dirB;
        }

        if (dotB >= dotB_max)
        {
            bboxData[3] = pos_O + dotB * dirB;
        }
    }
}
