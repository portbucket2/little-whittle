﻿using Unity.Entities;
using Unity.Collections;
using Unity.Mathematics;
using Unity.Burst;
using Unity.Jobs;
using UnityEngine;

[BurstCompile]
public struct Job5_Deform : IJobChunk
{
    /// <summary>
    /// O = centre of hit impact sphere
    /// C = current position of vertex
    /// P = point on sphere where vertex will travel to
    /// </summary>
    [ReadOnly] public float3 rayDirection;
    [ReadOnly] public float radius;
    [ReadOnly] public NativeArray<float> hitDepthAtFrame;
    [ReadOnly] public NativeArray<float3> hitPointAtFrame;

    public ArchetypeChunkComponentType<VertexPosition> vposType;
    public ArchetypeChunkComponentType<VertexNormal> vnorType;
    public ArchetypeChunkComponentType<VertexDeform> progType;

    [ReadOnly] public NativeArray<float3> interactionDirs;
    [ReadOnly] public int wCount;
    public NativeArray<float> floatData;//[0] bbox height, [1] bbox width, [2] dirtyVerts handled, [3] total verts handled, [4] overall progress,[5] max progress

    public NativeArray<int> cutoutData_assignment;
    public NativeArray<float3> cutOutPoints;
    public NativeArray<float3> cutOutPoints_nor;
    public NativeArray<float2> cutOutPoints_uv;
    public NativeArray<float> cutOutPoints_depth;

    //public NativeArray<int> chunkIndices;
    public NativeArray<int> chunkItemCount;
    public NativeArray<float> chunkProgress;


    public void Execute(ArchetypeChunk chunk, int chunkIndex, int firstEntityIndex)
    {
        NativeArray<VertexPosition> vposArray = chunk.GetNativeArray(vposType);
        NativeArray<VertexNormal> vnorArray = chunk.GetNativeArray(vnorType);
        NativeArray<VertexDeform> progArray = chunk.GetNativeArray(progType);
        
        float progresses=0;
        //Debug.LogFormat("{0} _ {1}",chunkIndex, progArray.Length);
        for (int i = 0; i < vposArray.Length; i++)
        {
            VertexPosition vpos = vposArray[i];
            VertexNormal vnor = vnorArray[i];
            VertexDeform defProg = progArray[i];


            if (hitDepthAtFrame[0] == float.MaxValue)
            {
                return;
            }
            else
            {
                float3 pos_O = hitPointAtFrame[0];
                float3 vec_V_O = pos_O - vpos.current;
                float dist_V_O = math.sqrt(vec_V_O.x * vec_V_O.x + vec_V_O.y * vec_V_O.y + vec_V_O.z * vec_V_O.z);
                float distRatio = dist_V_O / radius;

                if (distRatio < 1)
                {

                    floatData[3] = floatData[3] + 1;//total vcount
                    if (defProg.progress < 1) floatData[2] = floatData[2] + 1;//deformable vcount

                    float3 vec_I_F = vpos.final - vpos.initial;
                    float3 vec_C_F = vpos.final - vpos.current;
                    float dot_CF_IF = math.dot(vec_C_F, vec_I_F);
                    if (dot_CF_IF < 0)
                    {
                        vpos.current = vpos.final;
                        vnor.current = vnor.final;
                    }
                    else if (dot_CF_IF > 0)
                    {
                        float3 deform_dir = vec_I_F / defProg.mag_I_F;

                        float t1 = math.dot(vec_V_O, deform_dir);
                        float distance_from_dir_sqr = dist_V_O * dist_V_O - t1 * t1;
                        float t2 = math.sqrt(radius * radius - distance_from_dir_sqr);

                        float travelDist = t1 + t2;


                        float sqr_mag_C_F = vec_C_F.x * vec_C_F.x + vec_C_F.y * vec_C_F.y + vec_C_F.z * vec_C_F.z;
                        if (sqr_mag_C_F < travelDist * travelDist)
                        {
                            vpos.current = vpos.current + vec_C_F;
                            travelDist = math.sqrt(sqr_mag_C_F);
                        }
                        else
                        {
                            vpos.current = vpos.current + (deform_dir * travelDist);
                        }

                        //sDebug.Log("inside");

                        //Calculated progress towards goal;
                        vec_C_F = vpos.final - vpos.current;
                        dot_CF_IF = math.dot(vec_C_F, vec_I_F);
                        float mag_C_F = dot_CF_IF / defProg.mag_I_F;
                        defProg.progress =  1 - (mag_C_F / defProg.mag_I_F);

                        //floatData[4] = floatData[4] + math.clamp(defProg.progress,0,1)- math.clamp(progArray[i].progress,0,1);


                        //calculate normal
                        float3 tempV = pos_O - vpos.current;
                        float vec_C_O_mag = math.sqrt(tempV.x * tempV.x + tempV.y * tempV.y + tempV.z * tempV.z);
                        float3 hit_based_norm = math.normalize(math.lerp((tempV / vec_C_O_mag), -rayDirection, 0.5f));

                        float offset = 0.05f;
                        float3 vnorTarg;
                        if (defProg.progress < offset)
                        {

                            vnorTarg = math.lerp(vnor.initial, hit_based_norm, defProg.progress / offset);
                        }
                        else if (defProg.progress > 1 - offset)
                        {
                            vnorTarg = math.lerp(hit_based_norm, vnor.final, (defProg.progress - (1 - offset)) / offset);
                        }
                        else
                        {
                            vnorTarg = hit_based_norm;
                        }
                        vnor.current = math.lerp(vnor.current, vnorTarg, 1);
                    }

                    float W = floatData[1];
                    float gap = W / (wCount - 1);
                    float3 o = cutOutPoints[wCount * 2];
                    float3 wdir = interactionDirs[3];
                    float3 hdir = interactionDirs[2];
                    float3 v1 = vposArray[i].current;
                    float3 v2 = vpos.current;
                    float3 ov1 = v1 - o;
                    float3 ov2 = v2 - o;
                    int index1 = ((int)math.round(math.clamp(math.dot(ov1, wdir) / gap, 0, wCount - 1))) + 0*wCount;
                    
                    int index2 = ((int)math.round(math.clamp(math.dot(ov2, wdir) / gap, 0, wCount - 1))) + 1*wCount;
                    float dot1 = math.dot(ov1,hdir);
                    float dot1_min = math.dot(cutOutPoints[index1]-o,hdir);
                    if (dot1 <= dot1_min)
                    {
                        cutoutData_assignment[index1] = 0;
                        cutOutPoints[index1] = cutOutPoints[index1+2*wCount] +dot1*hdir;
                        cutOutPoints_nor[index1] = vnorArray[i].current;
                        cutOutPoints_uv[index1] = progArray[i].uv;
                        cutOutPoints_depth[index1] = progArray[i].progress;

                    }
                    float dot2 = math.dot(ov2, hdir);
                    float dot2_max = math.dot(cutOutPoints[index2] - o, hdir);
                    if (dot2 >= dot2_max)
                    {
                        cutoutData_assignment[index2] = 0;
                        cutOutPoints[index2] = cutOutPoints[index2 + 1* wCount] + dot2 * hdir;
                        cutOutPoints_nor[index2] = -vnor.current;
                        cutOutPoints_uv[index2] = defProg.uv;
                        cutOutPoints_depth[index2] = defProg.progress;
                    }




                    vposArray[i] = vpos;
                    vnorArray[i] = vnor;
                    progArray[i] = defProg;



                }
            }
            progresses += math.clamp(defProg.progress, 0, 1);
        }

        chunkProgress[chunkIndex] = progresses;
        chunkItemCount[chunkIndex] = vposArray.Length;

        //floatData[5] = floatData[5] + points;
        //floatData[4] = floatData[4] + progresses;

        //Debug.Log(points);

    }
}

