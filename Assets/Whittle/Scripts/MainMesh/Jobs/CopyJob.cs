﻿using UnityEngine;
using Unity.Entities;
using Unity.Collections;
using Unity.Mathematics;
using Unity.Burst;

[BurstCompile]
public struct CopyJob : IJobForEach<VertexIndex, VertexPosition, VertexNormal, VertexDeform>
{
    public NativeArray<Vector3> posA;
    public NativeArray<Vector3> norA;
    public NativeArray<Color> colA;
    public void Execute([ReadOnly] ref VertexIndex vi, [ReadOnly] ref VertexPosition vpos, [ReadOnly] ref VertexNormal vnor, [ReadOnly] ref VertexDeform vdef)
    {
        int i = vi.index;
        posA[i] = vpos.current;
        norA[i] = vnor.current;
        colA[i] = new Color(0, 0, 0, math.clamp(vdef.progress, 0, 1));
    }
}