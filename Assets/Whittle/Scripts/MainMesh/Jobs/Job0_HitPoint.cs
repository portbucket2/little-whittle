﻿using Unity.Entities;
using Unity.Collections;
using Unity.Mathematics;
using Unity.Burst;

[BurstCompile]
public struct Job0_HitPoint : IJobChunk
{
    public float3 rayOrigin;
    public float3 rayDirection;
    public float3 camDirection;
    public float detectionRadius;
    public NativeArray<float> hitDepthAtFrame;
    public NativeArray<float3> hitPointAtFrame;
    public NativeArray<float3> hitVertData;
    public NativeArray<int> hitOnCompletedVert;

    [ReadOnly] public ArchetypeChunkComponentType<VertexNormal> vnorType;
    [ReadOnly] public ArchetypeChunkComponentType<VertexPosition> vposType;
    [ReadOnly] public ArchetypeChunkComponentType<VertexDeform> progType;
    public void Execute(ArchetypeChunk chunk, int chunkIndex, int firstEntityIndex)
    {
        NativeArray<VertexPosition> vposArray = chunk.GetNativeArray(vposType);
        NativeArray<VertexNormal> vnorArray = chunk.GetNativeArray(vnorType);
        NativeArray<VertexDeform> progArray = chunk.GetNativeArray(progType);

        float radSqr = detectionRadius * detectionRadius;

        for (int i = 0; i < vposArray.Length; i++)
        {
            VertexPosition vpos = vposArray[i];
            VertexNormal vnor = vnorArray[i];

            float3 origin_point_vec = vpos.current - rayOrigin;
            float3 cross_product = math.cross(rayDirection, origin_point_vec);
            float cross_sqr_distance = cross_product.x * cross_product.x + cross_product.y * cross_product.y + cross_product.z * cross_product.z;

            if (cross_sqr_distance < radSqr)
            {

                float dot_distance_of_vert = math.abs(math.dot(rayDirection, origin_point_vec));


                float3 pos_O = rayOrigin + rayDirection * dot_distance_of_vert; //perpendicular intersection point is the ray origin
                float r = detectionRadius; //new hit pos will be at r distance from vert
                float3 pos_C = vpos.current;  //so, vert pos is the centre of sphere
                float3 vec_OC = pos_C - pos_O;
                float mag_CO_sqr = vec_OC.x * vec_OC.x + vec_OC.y * vec_OC.y + vec_OC.z * vec_OC.z;

                float t = math.sqrt(r * r - mag_CO_sqr);

                float3 hit_pos_for_current_vert = pos_O - rayDirection * t;
                float hit_depth_for_current_vert = dot_distance_of_vert - t;


                bool new_vert_completed = (progArray[i].progress >= 1);
                bool old_vert_completed = (hitOnCompletedVert[0]!=0);
                bool new_is_in_front = hit_depth_for_current_vert < hitDepthAtFrame[0];

                bool should_overtake;
                if (new_vert_completed == old_vert_completed)
                {
                    should_overtake = new_is_in_front;
                }
                else if (old_vert_completed) // new is not
                {
                    if (new_is_in_front)
                    {
                        should_overtake = true;
                    }
                    else
                    {
                        float3 normOld = math.normalize(hitVertData[1]);
                        float3 normNew = math.normalize(vnor.current);
                        float angle = math.acos( math.dot(normOld, normNew));
                        if (angle < 0) angle = -angle;

                        float dotCam = math.dot(camDirection,normNew);
                        
                        should_overtake = (angle <= math.PI/6) && (dotCam<=0); // should do a normal check
                    }
                }
                else//new vert is completed
                {
                    should_overtake = false;
                }

                if (should_overtake)
                {
                    hitVertData[0] = vpos.current;
                    hitVertData[1] = vnor.current;
                    hitDepthAtFrame[0] = hit_depth_for_current_vert;
                    hitPointAtFrame[0] = hit_pos_for_current_vert;
                    hitOnCompletedVert[0] = new_vert_completed?1:0;
                }
            }
        }
    }
}