﻿using Unity.Collections;
using Unity.Mathematics;
using Unity.Jobs;
using Unity.Burst;

[BurstCompile]
public struct Job4_PreDeform : IJob
{
    [ReadOnly] public int wCount;//5 as of now, always odd by convention
    public NativeArray<float> hitDepthAtFrame;
    public NativeArray<float3> bboxData;
    public NativeArray<float> floatData;//[0] bbox height, [1] bbox width, [2] dirtyVerts handled, [3] total verts handled, [4] overall progress,[5] max progress
    public NativeArray<float3> cutoutData;
    public NativeArray<int> cutoutData_assignment;


    public NativeArray<int> chunkItemCount;
    public NativeArray<float> chunkProgress;
    //public NativeArray<float3> hitPointAtFrame;

    //public NativeArray<float3> interactionDirs;

    public void Execute()
    {


        if (hitDepthAtFrame[0] == float.MaxValue)
        {
            return;
        }
        else
        {
            for (int i = 0; i < chunkItemCount.Length; i++)
            {
                chunkItemCount[i] = 0;
                chunkProgress[i] = 0;
            }


            float3 hVec = bboxData[1] - bboxData[0];
            float3 wVec = bboxData[3] - bboxData[2];
            float wVec_mag = math.sqrt(wVec.x*wVec.x +  wVec.y*wVec.y + wVec.z*wVec.z);
            float hVec_mag = math.sqrt(hVec.x * hVec.x + hVec.y * hVec.y + hVec.z * hVec.z);
            float3 wVec_norm = math.normalize(wVec);
            float3 hVec_norm = math.normalize(hVec);


            floatData[0] = hVec_mag;
            floatData[1] = wVec_mag;
            floatData[2] = 0;
            floatData[3] = 1;
            floatData[4] = 0;
            floatData[5] = 0;


            float bigNumber = 20;
            for (int i = 0; i < wCount; i++)
            {
                float offsetFromNegExtreme = i*(wVec_mag/(wCount-1));
                cutoutData[2 * wCount + i] = bboxData[2] + offsetFromNegExtreme * wVec_norm;
                cutoutData[0 * wCount + i] = cutoutData[2 * wCount + i] + hVec_norm * bigNumber;
                cutoutData[1 * wCount + i] = cutoutData[2 * wCount + i] - hVec_norm * bigNumber;
                cutoutData_assignment[0 * wCount + i] = -1;
                cutoutData_assignment[1 * wCount + i] = -1;
            }

            //bboxData[0] = hitPointAtFrame[0] + interactionDirs[2] * 10000;
            //bboxData[1] = hitPointAtFrame[0] - interactionDirs[2] * 10000;
            //bboxData[2] = hitPointAtFrame[0] + interactionDirs[3] * 10000;
            //bboxData[3] = hitPointAtFrame[0] - interactionDirs[3] * 10000;
        }


    }
}
