﻿using Unity.Collections;
using Unity.Mathematics;
using Unity.Jobs;
using Unity.Entities;
using Unity.Burst;

[BurstCompile]
public struct Job6_PostDeform : IJob
{
    [ReadOnly] public int wCount;//5 as of now, always odd by convention
    [ReadOnly] public NativeArray<float> hitDepthAtFrame;
    [ReadOnly] public NativeArray<float3> interactionDirs;

    public NativeArray<int> cutoutData_assignment;
    public NativeArray<float3> cutoutData;
    public NativeArray<float3> cutOutPoints_nor;
    public NativeArray<float2> cutOutPoints_uv;
    public NativeArray<float> cutOutPoints_depth;



    public void Execute()
    {
        if (hitDepthAtFrame[0] == float.MaxValue)
        {
            return;
        }
        else
        {

            float3 o = cutoutData[wCount * 2];
            float3 hdir = interactionDirs[2];

            for (int layer = 0; layer < 2; layer++)
            {

                for (int working_i = 0; working_i < wCount; working_i++)
                {
                    if (cutoutData_assignment[layer*wCount + working_i]<0)
                    {
                        int prev_i = -1;
                        int next_i = -1;
                        for (int i = working_i + 1; i < wCount; i++)
                        {
                            if (cutoutData_assignment[layer*wCount + i]==0)
                            {
                                next_i = i;
                                break;
                            }
                        }
                        for (int i = working_i - 1; i >= 0; i--)
                        {
                            if (cutoutData_assignment[layer*wCount + i]==0)
                            {
                                prev_i = i;
                                break;
                            }
                        }


                        int wi = layer * wCount + working_i;
                        int pi = layer * wCount + prev_i;
                        int ni = layer * wCount + next_i;

                        if (next_i < 0 && prev_i < 0)
                        {
                            throw new System.Exception("all items out of bounds");
                        }
                        else if (next_i < 0)
                        {
                            //cutoutData[layer*wCount + working_i] = cutoutData[layer*wCount + prev_i];

                            float cDot = math.dot(cutoutData[layer * wCount + prev_i] - o, hdir);
                            cutoutData[layer * wCount + working_i] = cutoutData[2 * wCount + working_i] + cDot * hdir;
                            cutOutPoints_nor[wi] = cutOutPoints_nor[pi];
                            cutOutPoints_uv[wi] = cutOutPoints_uv[pi];
                            cutOutPoints_depth[wi] = cutOutPoints_depth[pi];

                            cutoutData_assignment[layer * wCount + working_i] = 2;
                        }
                        else if (prev_i < 0)
                        {
                            //cutoutData[layer*wCount + working_i] = cutoutData[layer*wCount + next_i];

                            float cDot = math.dot(cutoutData[layer * wCount + next_i] - o, hdir);
                            cutoutData[layer * wCount + working_i] = cutoutData[2 * wCount + working_i] + cDot * hdir;
                            cutOutPoints_nor[wi] = cutOutPoints_nor[ni];
                            cutOutPoints_uv[wi] = cutOutPoints_uv[ni];
                            cutOutPoints_depth[wi] = cutOutPoints_depth[ni];

                            cutoutData_assignment[layer * wCount + working_i] = 3;
                        }
                        else
                        {
                            float prev_dist = working_i - prev_i;
                            float next_dist = next_i - working_i;
                            float lerpR = prev_dist / (prev_dist + next_dist);


                            float pDot = math.dot(cutoutData[pi] - o, hdir);
                            float nDot = math.dot(cutoutData[ni] - o, hdir);

                            float cDot = math.lerp(pDot,nDot,lerpR);

                            cutoutData[wi] = cutoutData[2 * wCount + working_i] + cDot * hdir;
                            cutOutPoints_nor[wi] = math.lerp(cutOutPoints_nor[pi], cutOutPoints_nor[ni],lerpR);
                            cutOutPoints_uv[wi] = math.lerp(cutOutPoints_uv[pi], cutOutPoints_uv[ni], lerpR);
                            cutOutPoints_depth[wi] = math.lerp(cutOutPoints_depth[pi], cutOutPoints_depth[ni], lerpR);
                            //cutoutData[layer*wCount + working_i] = math.lerp(cutoutData[layer*wCount + prev_i], cutoutData[layer*wCount + next_i], lerpR);

                            cutoutData_assignment[layer * wCount + working_i] = 6;
                        }
                        //cutoutData_assignment[layer*wCount + working_i] = true;

                    }
                }
            }


        }


    }
}

//[BurstCompile]
//public struct Job7_Progress : IJobForEach<VertexDeform>
//{
//    public NativeArray<float> floatData;//[0] bbox height, [1] bbox width, [2] dirtyVerts handled, [3] total verts handled, [4] overall progress,[5] max progress

//    public void Execute([ReadOnly] ref VertexDeform deform)
//    {
//        floatData[4] = floatData[4] + math.clamp(deform.progress, 0, 1);
//        floatData[5] = floatData[5] + 1;
        
//    }
//}