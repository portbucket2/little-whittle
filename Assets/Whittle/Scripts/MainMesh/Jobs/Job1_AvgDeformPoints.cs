﻿using Unity.Entities;
using Unity.Collections;
using Unity.Mathematics;
using Unity.Burst;
using UnityEngine;

[BurstCompile]
public struct Job1_AvgDeformPoints : IJobChunk
{
    /// <summary>
    /// O = centre of hit impact sphere
    /// C = current position of vertex
    /// P = point on sphere where vertex will travel to
    /// </summary>
    [ReadOnly] public float3 rayDirection;
    [ReadOnly] public float radius;
    [ReadOnly] public NativeArray<float> hitDepthAtFrame;
    [ReadOnly] public NativeArray<float3> hitPointAtFrame;

    [ReadOnly] public ArchetypeChunkComponentType<VertexPosition> vposType;
    [ReadOnly] public ArchetypeChunkComponentType<VertexDeform> progType;


    public NativeArray<int> intData;//access index 1 only to calculate deformed vert count
    public NativeArray<float3> interactionDir; //access index 1 and 2 for avg deform vec

    public void Execute(ArchetypeChunk chunk, int chunkIndex, int firstEntityIndex)
    {
        NativeArray<VertexPosition> vposArray = chunk.GetNativeArray(vposType);
        NativeArray<VertexDeform> progArray = chunk.GetNativeArray(progType);

        
        for (int i = 0; i < vposArray.Length; i++)
        {
            VertexPosition vpos = vposArray[i];
            VertexDeform defProg = progArray[i];
            if (hitDepthAtFrame[0] == float.MaxValue)
            {
                return;
            }
            else
            {
                float3 pos_O = hitPointAtFrame[0];
                float3 vec_V_O = pos_O - vpos.current;
                float dist_V_O = math.sqrt(vec_V_O.x * vec_V_O.x + vec_V_O.y * vec_V_O.y + vec_V_O.z * vec_V_O.z);
                float distRatio = dist_V_O / radius;

                if (distRatio < 1)
                {
                    float3 vec_I_F = vpos.final - vpos.initial;
                    float3 vec_C_F = vpos.final - vpos.current;
                    float dot_CF_IF = math.dot(vec_C_F, vec_I_F);
                    if (dot_CF_IF < 0)
                    {
                        vpos.current = vpos.final;

                    }
                    else if (dot_CF_IF > 0)
                    {
                        float3 deform_dir = vec_I_F / defProg.mag_I_F;

                        float t1 = math.dot(vec_V_O, deform_dir);
                        float distance_from_dir_sqr = dist_V_O * dist_V_O - t1 * t1;
                        float t2 = math.sqrt(radius * radius - distance_from_dir_sqr);

                        float travelDist = t1 + t2;


                        float sqr_mag_C_F = vec_C_F.x * vec_C_F.x + vec_C_F.y * vec_C_F.y + vec_C_F.z * vec_C_F.z;
                        if (sqr_mag_C_F < travelDist * travelDist)
                        {
                            vpos.current = vpos.current + vec_C_F;
                            travelDist = math.sqrt(sqr_mag_C_F);
                        }
                        else
                        {
                            vpos.current = vpos.current + (deform_dir * travelDist);
                        }

                        int N = intData[1];


                        interactionDir[1] = (interactionDir[1] * N + vposArray[i].current) / (N + 1); //avg start point
                        interactionDir[2] = (interactionDir[2] * N + vpos.current) / (N + 1);         //avg end point


                        intData[1] = N + 1;


                    }
                }               
            }




        }


    }
   
}
