﻿using Unity.Collections;
using Unity.Mathematics;
using Unity.Jobs;
using Unity.Burst;

[BurstCompile]
public struct Job2_InteractionDir : IJob
{
    public float3 rayDirection;
    public NativeArray<float> hitDepthAtFrame;
    public NativeArray<float3> hitVertAtFrame;
    public NativeArray<float3> hitPointAtFrame;

    public NativeArray<float3> interactionDirs;
    public NativeArray<float3> bboxData;//just for  cleanup
    public float3 defaultTravelDir;

    public void Execute()
    {
        interactionDirs[0] =  rayDirection;

        if (hitDepthAtFrame[0] == float.MaxValue)
        {
            return;
        }
        else
        {
            float3 avgDefVertIniPosDir = math.normalize(interactionDirs[1] - hitPointAtFrame[0]);


            if (hitDepthAtFrame[1] != float.MaxValue)
            {
                interactionDirs[1] = math.normalize(  hitPointAtFrame[0] - hitPointAtFrame[1]);
            }
            else
            {
                interactionDirs[1] = defaultTravelDir;
            }
            interactionDirs[2] = avgDefVertIniPosDir;
            interactionDirs[3] = math.normalize( math.cross(interactionDirs[1], interactionDirs[2]));



            bboxData[0] = hitPointAtFrame[0] + interactionDirs[2] * 10000;
            bboxData[1] = hitPointAtFrame[0] - interactionDirs[2] * 10000;
            bboxData[2] = hitPointAtFrame[0] + interactionDirs[3] * 10000;
            bboxData[3] = hitPointAtFrame[0] - interactionDirs[3] * 10000;
        }


    }
}
