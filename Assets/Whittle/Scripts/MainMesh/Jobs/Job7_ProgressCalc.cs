﻿using Unity.Entities;
using Unity.Collections;
using Unity.Mathematics;
using Unity.Burst;
using Unity.Jobs;
using UnityEngine;

[BurstCompile]
public struct Job7_ProgressCalc : IJob
{

    public NativeArray<int> chunkItemCount;
    public NativeArray<float> chunkProgress;
    public NativeArray<float> floatData;


    public void Execute()
    {

        int points = 0;
        float progresses = 0;
        for (int i = 0; i < chunkItemCount.Length; i++)
        {
            if (chunkItemCount[i] > 0)
            {
                points += chunkItemCount[i];
                progresses += chunkProgress[i];
            }
        }

        floatData[5] = floatData[5] + points;
        floatData[4] = floatData[4] + progresses;

        //Debug.Log(points);

    }
}
