﻿using UnityEngine;
using Unity.Entities;
using Unity.Collections;
public class VertEntSpwn : MonoBehaviour
{
    private Mesh iniMesh;
    private Mesh finMesh;
    


    int VC;
    Vector3[] vposI;
    Vector3[] normI;
    Vector3[] vposF;
    Vector3[] normF;
    Vector2[] uvI;

    public static World targetMeshWorld;
    public static EntityManager entityManager;

    void Start()
    {
        //Debug.Log("VES S");
        Application.targetFrameRate = 3000;

        if (targetMeshWorld == null)
        {
            targetMeshWorld = World.Active;
            entityManager = targetMeshWorld.EntityManager;

        }
        iniMesh = MainMeshReformer.instance.meshFilterOut.mesh;
        finMesh = MainMeshReformer.instance.meshFilterIn.mesh;

        iniMesh = Instantiate(iniMesh);
        vposI = iniMesh.vertices;
        normI = iniMesh.normals;
        
        finMesh = Instantiate(finMesh);
        vposF = finMesh.vertices;
        normF = finMesh.normals;

        uvI = iniMesh.uv;

        VC = vposI.Length;



        GenerateWorkVertEntities();

        MainMeshReformer.instance.Init(VC,iniMesh);

        LevelPrefabManager.onUnload += OnUnload;
    }

    private void OnUnload()
    {
        LevelPrefabManager.onUnload -= OnUnload;

        //Debug.Log("VES D");
        NativeArray<Entity> entities = entityManager.GetAllEntities();
        for (int i = 0; i < entities.Length; i++)
        {
            entityManager.DestroyEntity(entities[i]);
        }
    }
    
    private void GenerateWorkVertEntities()
    {
        EntityArchetype vertexArchetype = entityManager.CreateArchetype(
           typeof(VertexIndex),
           typeof(VertexDeform),
           typeof(VertexPosition),
           typeof(VertexNormal)
           );

        NativeArray<Entity> entities = new NativeArray<Entity>(VC, Allocator.Persistent);
        for (int i = 0; i < VC; i++)
        {
            entities[i] = entityManager.CreateEntity(vertexArchetype);
            entityManager.SetComponentData(entities[i], new VertexPosition
            {
                current = vposI[i],
                initial = vposI[i],
                final = vposF[i]
            }); 
            entityManager.SetComponentData(entities[i], new VertexNormal
            {
                current = normI[i],
                initial = normI[i],
                final = normF[i]
            });

            entityManager.SetComponentData(entities[i], new VertexIndex
            {
                index=i
            }); 
            entityManager.SetComponentData(entities[i], new VertexDeform
            {
                progress = 0,
                mag_I_F = Vector3.Distance(vposF[i],vposI[i]),
                uv = uvI[i]
            });
        }
        entities.Dispose();
    }
}
