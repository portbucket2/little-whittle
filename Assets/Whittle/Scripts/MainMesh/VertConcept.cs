﻿using Unity.Entities;
using Unity.Mathematics;

public struct VertexIndex : IComponentData
{
    public int index;
}
public struct VertexPosition : IComponentData
{
    public float3 initial;
    public float3 final;
    public float3 current;
}
public struct VertexNormal : IComponentData
{
    public float3 initial;
    public float3 final;
    public float3 current;
}
public struct VertexDeform : IComponentData
{
    public float2 uv;
    public float mag_I_F;
    public float progress;
}
