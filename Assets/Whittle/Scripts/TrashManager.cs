﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrashManager : MonoBehaviour
{
    public static TrashManager instance;

    [SerializeField] bool dontEjectWithPhysics = false;
    float ejectForce = 0.75f;
    [SerializeField] Vector3 targetRelativeSpeed = Vector3.zero;
    [SerializeField] Transform floor;
    [SerializeField] Transform trashDiscardPoint;




    private void Awake()
    {
        instance = this;
        StartCoroutine(SuckIn());
    }

    void Update()
    {
        for (int i = oldPeels.Count - 1; i >= 0; i--)
        {
            if (trashDiscardPoint && oldPeels[i].root.position.y < trashDiscardPoint.position.y)
            {
                oldPeels[i].TrashItems();
                oldPeels.RemoveAt(i);
            }
        }
        this.transform.Rotate((RotationSpeedManager.instance.ChosenSpeed - targetRelativeSpeed) * Time.deltaTime);
    }




    IEnumerator SuckIn()
    {
        while (true)
        {
            foreach (var item in oldPeels)
            {
                //if (!item.root) continue;
                if (Time.time < item.ejectTime + 0.75f)
                {
                    float maxY = item.ejectYpos - floor.position.y;
                    float y = item.root.transform.position.y - floor.position.y;

                    float scaleM = Mathf.Lerp(0.6f, 1f, Mathf.Clamp01(y / maxY));
                    if (item.root.localScale.sqrMagnitude > scaleM * scaleM)
                        item.root.localScale = Vector3.one * scaleM;
                }
                else if (Time.time > item.ejectTime + 0.9f)
                {

                    if (!item.rgbd.isKinematic)
                    {
                        item.rgbd.isKinematic = true;
                        item.UnloadPoolColliders();
                    }

                    float suckRate = Mathf.Lerp(0.02f, 0.2f, Mathf.Clamp01(item.fibers.Count / 350));
                    item.root.localPosition += new Vector3(0, -suckRate * Time.deltaTime, 0);
                }

            }
            yield return null;
        }
    }


    List<PeelBehaviour> oldPeels = new List<PeelBehaviour>();
    public void AddPeelToTrash(PeelBehaviour peelBuilder)
    {
        //HERE
        peelBuilder.EjectPeel(disablePhysics: dontEjectWithPhysics, ejectForce: ejectForce);
        peelBuilder.root.SetParent(transform);

        oldPeels.Add(peelBuilder);
    }
    private void OnDestroy()
    {
        foreach (var peel in oldPeels)
        {
            peel.TrashItems();
        }
    }

}