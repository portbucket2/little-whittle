﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class InputRayManager : MonoBehaviour
{
    Camera cam;
    //public float rayDetectionRadius;
    public Transform travelDirTr;

    public static InputRay inputRay;

    public float rayLength = 0;

    public KeyCode centreCastKey = KeyCode.Space;

    public float offsetYFromTouch=200;

    private void Start()
    {
        inputRay = null;
        cam = this.GetComponent<Camera>();
    }

    void Update()
    {
        if (Input.GetMouseButton(0)&& MainMeshReformer.meshTrans)
        {
            Ray ray = cam.ScreenPointToRay(Input.mousePosition + new Vector3(0, offsetYFromTouch*Screen.height/900, 0));
            Debug.DrawRay(ray.origin, ray.direction * rayLength);
            inputRay = new InputRay(ray, MainMeshReformer.meshTrans, travelDirTr.forward,cam.transform.forward);
            //Debug.Log(Input.mousePosition + new Vector3(0, Screen.width / 5.0f, 0));
        }
        else if (Input.GetKey(centreCastKey) && MainMeshReformer.meshTrans)
        {
            Ray ray = cam.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2, 0));
            Debug.DrawRay(ray.origin, ray.direction * rayLength);
            inputRay = new InputRay(ray, MainMeshReformer.meshTrans, travelDirTr.forward, cam.transform.forward);
            //Debug.Log(new Vector3(Screen.width / 2, Screen.height / 2, 0));
        }
        else
        {
            inputRay = null;
        }
    }
}

public class InputRay
{
    public Vector3 origin;
    public Vector3 direction;
    public Vector3 defaultTravelDir;
    public Vector3 camDirection;
    //public float detectionRadius;
    public InputRay(Ray r, Transform tr, Vector3 defaultTravelDir, Vector3 camDir)
    {
        origin = tr.InverseTransformPoint( r.origin);
        direction = tr.InverseTransformDirection( r.direction).normalized;
        camDirection = tr.InverseTransformDirection(camDir).normalized;
        this.defaultTravelDir = tr.InverseTransformDirection(defaultTravelDir).normalized;
        //this.detectionRadius = detectionRadius;
    }
}

