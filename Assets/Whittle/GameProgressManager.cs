﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class GameProgressManager : MonoBehaviour
{
    public static GameProgressManager instance;
    float topLimit = 612;
    float botLimit = 17;
    private int _starCount = 0;
    public int StarCount
    {
        get { return _starCount; }
        set
        {
            if (value >= 3 && _starCount < 3)
            {
                star3.SetTrigger("go");
            }
            if (value >= 2 && _starCount < 2)
            {
                star2.SetTrigger("go");
            }
            if (value >= 1 && _starCount < 1)
            {
                star1.SetTrigger("go");
            }
            else if (value == 0)
            {
                star1.ResetTrigger("go");
                star2.ResetTrigger("go");
                star3.ResetTrigger("go");

                star1.SetTrigger("reset");
                star2.SetTrigger("reset");
                star3.SetTrigger("reset");
            }
            _starCount = value;
        }
    }
    string progressText
    {
        get
        {
            switch (StarCount)
            {
                case 1:
                    return "Great!";
                case 2:
                    return "Awesome!";
                case 3:
                    return "Excellent!";
                default:
                    return "";
            }
        }
    }
    


    
    public RectTransform m1;
    public RectTransform m2;
    public RectTransform m3;
    public Animator star1;
    public Animator star2;
    public Animator star3;

    public Image progressSlider;
    public Text progressRelatedText;
    public Animator progressRelatedTextAnim;
    
    

    //public GameObject confetti;
    public ParticleSystem confettiSystem1;
    public ParticleSystem confettiSystem2;
   

    void Awake()
    {
        instance = this;
        LevelPrefabManager.onLoad += OnNewPrefabLoad;
        LevelPrefabManager.onUnload += OnPrefabUnload;
        LevelPrefabManager.onProgressChanged += OnProgress;
        LevelPrefabManager.onStarCountChanged += OnStar;
    }


    void OnDestroy()
    {
        LevelPrefabManager.onLoad -= OnNewPrefabLoad;
        LevelPrefabManager.onUnload -= OnPrefabUnload;
        LevelPrefabManager.onProgressChanged -= OnProgress;
        LevelPrefabManager.onStarCountChanged -= OnStar;
    }

    bool completed;
    void OnNewPrefabLoad()
    {
        completed = false;
        StarCount = 0;
        //PhaseManager.instance.forceUseDefaultCamTrans = false;
        progressSlider.fillAmount = 0;
        RotationSpeedManager.instance.BreakRotation(0.75f, true);


    }
    void OnPrefabUnload()
    {
        OnStar(0);
        OnProgress(0);
    }



    void OnStar(int star)
    {
        if (StarCount != star) StarCount = star;
        progressRelatedTextAnim.SetTrigger("shake");
        progressRelatedText.text = progressText;
    }

    void OnProgress(float progress)
    {
        progressSlider.fillAmount = progress;
        bool enoughToGoToNextPhase = progress >= 1;

        if (!completed)
        {
            if (enoughToGoToNextPhase)
            {
                DoCompletion();
                return;
            }
#if UNITY_EDITOR
            if (Input.GetKeyDown(KeyCode.End))
            {
                DoCompletion();
                return;
            }
#endif
        }

    }

    void DoCompletion()
    {
        completed = true;
        DoCompletionCelebration();
        LevelPrefabManager.currentLevel.OnComplete(true, "Well Done", null);
    }
    void DoCompletionCelebration()
    {
        if(confettiSystem1) confettiSystem1.Play();
        if(confettiSystem2) confettiSystem2.Play();
        //if (hapticSoundSource.hapticEnabled)//add haptic setting
        {
#if UNITY_EDITOR
            Debug.Log("VIBRATTTTIIIINNNGGGGG");
#else
            Handheld.Vibrate();
#endif
        }
    }



}